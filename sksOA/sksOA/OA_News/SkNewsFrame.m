//
//  SkNewsFrame.m
//  sksOA
//
//  Created by pandom on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "SkNewsFrame.h"
#import "SkNews.h"
@class SkNews;

#define HeaderViewHeight 40
#define ContentViewHeight 95
#define LeftX 15
#define JianJu 15

@implementation SkNewsFrame

-(void)setNews:(SkNews *)news
{
    _news = news;
    
    _headerViewF = CGRectMake(LeftX, JianJu, 320-2*LeftX, HeaderViewHeight);
    
    _departLabelF = CGRectMake(0, 0, 20, HeaderViewHeight);
    
    _labelF= CGRectMake(_headerViewF.size.width-20, 0, 20, 10);
    
    _titleLabelF = CGRectMake(CGRectGetMaxX(_departLabelF), 0, _headerViewF.size.width-_departLabelF.size.width-_labelF.size.width, HeaderViewHeight*0.7);
    
    _timeLabelF = CGRectMake(0.6*(_headerViewF.size.width - _departLabelF.size.width), _titleLabelF.size.height, 0.4*(_headerViewF.size.width - _departLabelF.size.width), HeaderViewHeight-_titleLabelF.size.height);
    
    CGFloat contentX = JianJu;
    CGFloat contentY = CGRectGetMaxY(_headerViewF)+JianJu;
    CGFloat contentW = _headerViewF.size.width;
    
    
    NSStringDrawingOptions option = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    //NSStringDrawingTruncatesLastVisibleLine如果文本内容超出指定的矩形限制，文本将被截去并在最后一个字符后加上省略号。 如果指定了NSStringDrawingUsesLineFragmentOrigin选项，则该选项被忽略 NSStringDrawingUsesFontLeading计算行高时使用行间距。（译者注：字体大小+行间距=行高）
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12.0f] forKey:NSFontAttributeName];
    CGSize size = CGSizeMake(contentW, 92);
    CGRect rect = [news.content boundingRectWithSize:size options:option attributes:attributes context:nil];
    
    _contentLabelF = CGRectMake(0, 0, contentW, rect.size.height);
    _contentViewF = CGRectMake(contentX, contentY, _headerViewF.size.width,rect.size.height);
    
    CGFloat lineX = 0;
    CGFloat lineY = CGRectGetMaxY(_contentViewF)+JianJu;
    _lineViewF = CGRectMake(lineX, lineY, 320, 1);
    

    _cellHeight = CGRectGetMaxY(_lineViewF);


}

@end
