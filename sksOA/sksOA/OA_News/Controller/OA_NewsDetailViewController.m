//
//  OA_NewsDetailViewController.m
//  sksOA
//
//  Created by pandom on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_NewsDetailViewController.h"
#define TitleFontSize 14.0f
#define DepartFontSize 8.0f
#define ContentFontSize 12.0f
#define JianJu 15
@class SkNews;

@interface OA_NewsDetailViewController ()
{
    UILabel *_titleLabel;
    UILabel *_departLabel;
    UILabel *_label;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
}
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic) UIScrollView *newsScrollView;
@end

@implementation OA_NewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _newsScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-65)];
    _newsScrollView.contentSize = _newsScrollView.frame.size;
    _newsScrollView.showsVerticalScrollIndicator = YES;
    _newsScrollView.showsHorizontalScrollIndicator = NO;
    _newsScrollView.bounces = NO;
    
    [self.view addSubview:_newsScrollView];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self setFrame];
}


-(void)setNews:(SkNews *)news
{
    _news = news;
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont systemFontOfSize:TitleFontSize weight:2.0f];
    _titleLabel.text = _news.title;
    _titleLabel.numberOfLines = 0;
    
    
    _departLabel = [[UILabel alloc]init];
    _departLabel.font = [UIFont systemFontOfSize:DepartFontSize weight:0.5f];
    _departLabel.text = [NSString stringWithFormat:@"来源：%@",_news.departmentName];
    
    
    _label = [[UILabel alloc]init];
    _label.font = [UIFont systemFontOfSize:DepartFontSize weight:0.5f];
    _label.text = [NSString stringWithFormat:@"标签：%@",_news.label ];
    if (_news.label == nil) {
        _label.hidden = YES;
    }
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.font = [UIFont systemFontOfSize:DepartFontSize weight:0.5f];
    _timeLabel.text = _news.createTime;
    _timeLabel.textAlignment = NSTextAlignmentRight;
    
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.font = [UIFont systemFontOfSize:ContentFontSize weight:1.0f];
    _contentLabel.text = _news.content;
    _contentLabel.numberOfLines = 0;
    
}

-(void)setFrame
{
    
    NSStringDrawingOptions option = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    CGFloat titleX = JianJu;
    CGFloat titleY = JianJu;
    CGFloat titleW = self.newsScrollView.contentSize.width - 2*JianJu;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:TitleFontSize] forKey:NSFontAttributeName];
    CGSize size = CGSizeMake(titleW, CGFLOAT_MAX);
    CGRect rect = [_news.title boundingRectWithSize:size options:option attributes:attributes context:nil];
    CGFloat titleH = rect.size.height;
    _titleLabelF = CGRectMake(titleX, titleY, titleW, titleH);
    _titleLabel.frame = _titleLabelF;
    [self.newsScrollView addSubview:_titleLabel];
    
    
    CGFloat departX = titleX;
    CGFloat departY = CGRectGetMaxY(_titleLabelF)+JianJu;
    _departLabelF = CGRectMake(departX, departY, 100, JianJu);
    _departLabel.frame = _departLabelF;
    [self.newsScrollView addSubview:_departLabel];
    
    CGFloat labelX = titleX;
    CGFloat labelY = CGRectGetMaxY(_departLabelF)+JianJu*0.5;
    _labelF = CGRectMake(labelX, labelY, 100, JianJu);
    _label.frame = _labelF;
    
    [self.newsScrollView addSubview:_label];
    
    CGFloat timeX = self.newsScrollView.contentSize.width - 200;
    CGFloat timeY = CGRectGetMaxY(_departLabelF)+JianJu*0.5;
    _timeLabelF = CGRectMake(timeX, timeY, 200-JianJu, JianJu);
    _timeLabel.frame = _timeLabelF;
    [self.newsScrollView addSubview:_timeLabel];
    
    CGFloat contentX = titleX;
    CGFloat contentY = CGRectGetMaxY(_labelF)+JianJu;
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:ContentFontSize] forKey:NSFontAttributeName];
    CGSize size2= CGSizeMake(titleW, CGFLOAT_MAX);
    CGRect rect2 = [_news.content boundingRectWithSize:size2 options:option attributes:attributes2 context:nil];
    _contentLabelF =  CGRectMake(contentX, contentY, titleW, rect2.size.height);
    _contentLabel.frame = _contentLabelF;
    [self.newsScrollView addSubview:_contentLabel];
    
    
    CGFloat tempHeight = CGRectGetMaxY(_contentLabelF)+JianJu*3;
    if (tempHeight > self.newsScrollView.contentSize.height) {
        self.newsScrollView.contentSize = CGSizeMake(self.newsScrollView.contentSize.width ,tempHeight);
    }
    
    
}

- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
