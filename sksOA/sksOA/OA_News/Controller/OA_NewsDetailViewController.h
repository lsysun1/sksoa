//
//  OA_NewsDetailViewController.h
//  sksOA
//
//  Created by pandom on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
#import "SkNews.h"

@interface OA_NewsDetailViewController : OA_BaseViewController

@property(copy,nonatomic) SkNews *news;

@property (nonatomic,assign,readonly) CGRect departLabelF;
@property (nonatomic,assign,readonly) CGRect titleLabelF;
@property (nonatomic,assign,readonly) CGRect timeLabelF;
@property (nonatomic,assign,readonly) CGRect labelF;
@property (nonatomic,assign,readonly) CGRect contentLabelF;

@property (nonatomic,assign,readonly) CGFloat cellHeight;

@end
