//
//  SksNewsCell.m
//  sksOA
//
//  Created by pandom on 15/5/7.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "SksNewsCell.h"
#import "SkNews.h"
#import <UIKit/NSStringDrawing.h>
#define HeaderViewHeight 40
#define ContentViewHeight 95
#define LeftX 15
#define JianJu 15


@interface SksNewsCell()
{
    UIView *_headerView;
    UILabel *_titleLabel;
    UIButton *_departLabel;
    UILabel *_label;
    UILabel *_timeLabel;
    
    UIView *_contentView;
    UILabel *_contentLabel;
    UIView *_line;
}
@end

@implementation SksNewsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self ) {
        //添加内部控件
        
//        5.创建headerView
        _headerView = [[UIView alloc]init];
        [_headerView setBackgroundColor:[UIColor colorWithRed:1.5 green:1 blue:2 alpha:0.2]];
        [self.contentView addSubview:_headerView];
        
//        1.创建titleLabel并添加到headerView
        _titleLabel = [[UILabel alloc ]init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14.0f]; //UILabel的字体大小
        _titleLabel.numberOfLines = 0; //必须定义这个属性，否则UILabel不会换行
        _titleLabel.textAlignment = NSTextAlignmentLeft; //文本对齐方式
        [_headerView addSubview:_titleLabel];
        
//        2.创建departLabel并添加到headerView
        _departLabel = [[UIButton alloc ]init];
//        _departLabel.titleLabel.textColor = [UIColor whiteColor];
        [_departLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _departLabel.backgroundColor=[UIColor blackColor];
        _departLabel.titleLabel.font = [UIFont boldSystemFontOfSize:8.0f]; //UILabel的字体大小
//        _departLabel.textAlignment = NSTextAlignmentCenter; //文本对齐方式
         _departLabel.titleLabel.numberOfLines = 0; //必须定义这个属性，否则UILabel不会换行
        _departLabel.tag= 101;
        [_headerView addSubview:_departLabel];
        
//        3.创建label公告并添加到headerView
        _label = [[UILabel alloc ]init];
        _label.textColor = [UIColor whiteColor];
        _label.backgroundColor=[UIColor blackColor];
        _label.font = [UIFont boldSystemFontOfSize:8.0f]; //UILabel的字体大小
        _label.textAlignment = NSTextAlignmentCenter; //文本对齐方式
        [_headerView addSubview:_label];
       
        
        
//        4.创建时间timeLabel并添加到headerView
        _timeLabel = [[UILabel alloc ]init];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont boldSystemFontOfSize:10.0f];
        _timeLabel.textAlignment = NSTextAlignmentCenter; //文本对齐方式
        [_headerView addSubview:_timeLabel];
        
        
//        8.创建contentView
         _contentView = [[UIView alloc]init];
        [_contentView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.2]];
        [self.contentView addSubview:_contentView];
        
//        7.创建contentLabel并添加到contentView
        
        _contentLabel = [[UILabel alloc ]init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        _contentLabel.textAlignment = NSTextAlignmentLeft; //文本对齐方式
        _contentLabel.tag = 102;
        [_contentView addSubview:_contentLabel];
        
//        8.创建下划
        _line = [[UIView alloc]init];
        [_line setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.2]];
        [self.contentView addSubview:_line];
    }
    return self;
}


-(void)setNewsFrame:(SkNewsFrame *)newsFrame
{
    _newsFrame = newsFrame;
//    1.设置新闻高度
    [self settingSubViewFrame:newsFrame];
//    2.设置新闻数据
    [self settingData];
}

-(void)settingSubViewFrame:(SkNewsFrame *)newsFrame
{
    _headerView.frame = _newsFrame.headerViewF;
    
    
    _departLabel.frame = _newsFrame.departLabelF;
   
    
    
    _label.frame = _newsFrame.labelF;
    
    _titleLabel.frame = _newsFrame.titleLabelF;
    
    _timeLabel.frame = _newsFrame.timeLabelF;
    
   
    _contentLabel.frame = _newsFrame.contentLabelF;
    _contentView.frame = _newsFrame.contentViewF;
    

    _line.frame = _newsFrame.lineViewF;
    
}

-(void)settingData
{
    SkNews *news = _newsFrame.news;
    
    [_departLabel setTitle:news.departmentName forState:UIControlStateNormal];
    
    _timeLabel.text = news.createTime;
    
    _titleLabel.text = news.title;
    
    NSString *str = [NSString stringWithFormat:@"%@" ,news.label ];
    NSLog(@"%@",str);
    if (news.label == nil || [news.label isEqualToString:@""]) {
        _label.hidden = YES;
    }else{
        _label.text = news.label;
    }
    _contentLabel.text = news.content;
//    [_contentLabel setTitle:news.content forState:UIControlStateNormal];
}
@end







