//
//  OA_NewsViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"

@interface OA_NewsViewController : OA_BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *SksNewsTableView;

@end
