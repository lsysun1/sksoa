//
//  OA_NewsViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_NewsViewController.h"
#import "SksNewsCell.h"
#import "SkNews.h"
#import "SkNewsFrame.h"
#import "OA_NewsDetailViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MJRefresh.h"

#define UP 1
#define DOWN 0


@interface OA_NewsViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString * _maxNumber;
    NSMutableArray *_newsFrameArray;
    NSMutableArray *_totalFrameArray;
    long departTag;

}
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation OA_NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_SksNewsTableView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]];

    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.SksNewsTableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [self.SksNewsTableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadOldData)];
    
    _totalFrameArray = [[NSMutableArray alloc]init];
    _newsFrameArray = [[NSMutableArray alloc]init];
    [self sendRequestByNet:@"0" setDepartId:@"0" setLoadDirection:UP];
}

-(void)loadNewData
{
    [self.SksNewsTableView.header beginRefreshing];
    [self sendRequestByNet:_maxNumber setDepartId:@"0" setLoadDirection:UP];
    [self.SksNewsTableView.header endRefreshing];
}

-(void)loadOldData
{
    [self.SksNewsTableView.footer beginRefreshing];
    [self sendRequestByNet:_maxNumber setDepartId:@"0" setLoadDirection:DOWN];
    [self.SksNewsTableView.footer endRefreshing];
}

#pragma mark - 数据请求
- (void) sendRequestByNet:(NSString *) beginNum setDepartId:(NSString *)departmentId setLoadDirection:(NSInteger) direction
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/SKSnews";
    
    
    if (_newsFrameArray.count > 0 && direction == UP) {
        [_newsFrameArray removeAllObjects];
        beginNum = @"0";
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"req_number"] = beginNum;
    parameters[@"departmentID"] = departmentId;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];

    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resArr = responseObject;
        
        for(NSDictionary *dict in resArr){
            SkNewsFrame *frame = [[SkNewsFrame alloc]init];
            SkNews *news = [SkNews newsWithDict:dict];
            frame.news = news;
            [_newsFrameArray addObject:frame];
        }
        
        [_totalFrameArray removeAllObjects];
        for (int i=0; i<_newsFrameArray.count; i++) {
            [_totalFrameArray addObject:_newsFrameArray[i]];
        }
        int count = (int)_totalFrameArray.count;
        if (count > 0) {
            _maxNumber = [[_newsFrameArray[count-1] news] nid];
        }else{
            _maxNumber = @"0";
        }
        
        [self.SksNewsTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];

}



#pragma mark - UITableViewDelegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (NSInteger)_newsFrameArray.count ;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *resuseId = @"newsCell";
    SksNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:resuseId];
    
    if (cell == nil) {
        cell = [[SksNewsCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:resuseId];
    }
    if (_newsFrameArray.count>0) {
        cell.newsFrame = _newsFrameArray[indexPath.row];
        [cell setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIButton *label = (UIButton *)[cell viewWithTag:101];
        NSString * departmentId = [_newsFrameArray[indexPath.row] news].departmentID;
        [label addTarget:self action:@selector(classifyByDempartment:) forControlEvents:UIControlEventTouchUpInside];
        [label setTag:[departmentId intValue] ];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0;
    if (_newsFrameArray.count>0) {
        height = [_newsFrameArray[indexPath.row] cellHeight];
    }
    return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%zi",indexPath.row);
    OA_NewsDetailViewController *detailController = [[OA_NewsDetailViewController alloc]init];
    detailController.news = [_newsFrameArray[indexPath.row] news];
    [self.navigationController pushViewController:detailController animated:YES];
}

-(void) classifyByDempartment:(id)sender
{
    NSLog(@"分类信息: %zi",[sender tag]);
    [_newsFrameArray removeAllObjects];
    NSString *departID = [NSString stringWithFormat:@"%zi",[sender tag] ];
    
    if ([departID isEqualToString:departID]) {
        for (int j=0; j<_totalFrameArray.count; j++) {
            [_newsFrameArray addObject:_totalFrameArray[j]];
        }
    }else{
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        for (int i = 0;i<_totalFrameArray.count;i++) {
            SkNewsFrame *frame = [_totalFrameArray objectAtIndex:i];
            if ([frame.news.departmentID isEqualToString:departID]) {
                [tempArray addObject:frame];
            }
        }
        _newsFrameArray = [[NSMutableArray alloc]initWithArray:tempArray copyItems:YES];
    }
    
    [self.SksNewsTableView reloadData];
}


- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
