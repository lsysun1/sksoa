//
//  SksNewsCell.h
//  sksOA
//
//  Created by pandom on 15/5/7.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkNewsFrame.h"

@interface SksNewsCell : UITableViewCell

@property (nonatomic,strong)SkNewsFrame *newsFrame;


@end
