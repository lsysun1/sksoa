//
//  SkNews.h
//  sksOA
//
//  Created by pandom on 15/5/6.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SkNews : NSObject

@property(copy,nonatomic) NSString *nid;
@property(copy,nonatomic) NSString *title;
@property(copy,nonatomic) NSString *content;
@property(copy,nonatomic) NSString *departmentID;
@property(copy,nonatomic) NSString *departmentName;
@property(copy,nonatomic) NSString *label;
@property(copy,nonatomic) NSString *createTime;

-(id)initWithDict:(NSDictionary *) dict;
+(id)newsWithDict:(NSDictionary *) dict;
@end
