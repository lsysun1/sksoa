//
//  SkNewsFrame.h
//  sksOA
//
//  Created by pandom on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//
@class SkNews;
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface SkNewsFrame : NSObject
@property (nonatomic,assign,readonly) CGRect departLabelF;
@property (nonatomic,assign,readonly) CGRect titleLabelF;
@property (nonatomic,assign,readonly) CGRect headerViewF;
@property (nonatomic,assign,readonly) CGRect timeLabelF;
@property (nonatomic,assign,readonly) CGRect labelF;
@property (nonatomic,assign,readonly) CGRect contentLabelF;
@property (nonatomic,assign,readonly) CGRect contentViewF;
@property (nonatomic,assign,readonly) CGRect lineViewF;

@property (nonatomic,assign,readonly) CGFloat cellHeight;

@property (nonatomic,copy)SkNews *news;

@end
