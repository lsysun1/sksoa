//
//  SksNewsDao.m
//  sksOA
//
//  Created by pandom on 15/5/10.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "SksNewsDao.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"


@class SkNews;
@implementation SksNewsDao

+(NSMutableArray *) getSksNewsByNet:(NSInteger *) beginNum setDepartId:(NSInteger *)departmentId
{
    NSMutableArray *array = [[NSMutableArray alloc ]init] ;
    if(beginNum == nil)
        beginNum = 0;
    
    if(departmentId == nil)
        departmentId = 0;
    
    NSString *str=[NSString stringWithFormat:@"http://182.140.244.136:8090/SKSAO/SKSnews?req_number=%zi&departmentID=%zi",beginNum,departmentId];
    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *html = operation.responseString;
        NSData* data=[html dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *dict=[NSJSONSerialization  JSONObjectWithData:data options:0 error:nil];
        NSLog(@"获取到的数据为：%@",dict);
        [array addObjectsFromArray:dict];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"发生错误！%@",error);
    }];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];

    
    return array;
}


-(NSMutableArray *) getSksNewsByNet
{
    NSMutableArray *array = [[NSMutableArray alloc ]init] ;
   
    NSString *getUrl = @"http://182.140.244.136:8090/SKSAO/SKSnews";
    
    NSDictionary *parameters = @{@"req_number": @"1",
                                 @"departmentID":@"0"};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //方法一：
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];

    //注意：默认的Response为json数据
    //    [manager setResponseSerializer:[AFXMLParserResponseSerializer new]];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];//使用这个将得到的是NSData
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//使用这个将得到的是JSON
    
    //注意：此行不加也可以
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain; charset=utf-8"];
    //    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    
    
    //SEND YOUR REQUEST
    [manager GET:getUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id resopnseObject){
        NSLog(@"JSON: %@", resopnseObject);
        NSLog(@"first %@",[resopnseObject objectAtIndex:0]);
        [array addObjectsFromArray:(NSArray *)resopnseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    return array;
    
//    [newsArray addObjectsFromArray:[SksNewsDao getSksNewsByNet:self.beginNum setDepartId:self.departmentId]];
//    self.beginNum = (NSInteger *)newsArray.count;
}


@end
