//
//  SkNews.m
//  sksOA
//
//  Created by pandom on 15/5/6.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "SkNews.h"

@implementation SkNews

@synthesize nid,content,title,departmentID,departmentName,label,createTime;

-(id)initWithDict:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.nid = [NSString stringWithFormat:@"%@",dict[@"news_id"]];
        self.title = [NSString stringWithFormat:@"%@",dict[@"title"]];
        self.content = [NSString stringWithFormat:@"%@",dict[@"content"]];
        self.departmentID = [NSString stringWithFormat:@"%@",dict[@"departmentID"]];
        self.departmentName = [NSString stringWithFormat:@"%@",dict[@"departmentName"]];
        self.createTime = [[NSString stringWithFormat:@"%@",dict[@"news_date"]] substringToIndex:19];
        NSLog(@"dict[label] = %@",dict[@"label"]);
        
        self.label = [NSString stringWithFormat:@"%@",dict[@"label"]];
        if (self.label==nil || [self.label isEqualToString:@"<null>"]) {
            self.label = @"";
        }
        
    }
    return self;
}

+(id)newsWithDict:(NSDictionary *)dict
{
    return [[SkNews alloc]initWithDict:dict];
}


@end
