//
//  SksNewsDao.h
//  sksOA
//
//  Created by pandom on 15/5/10.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SksNewsDao : NSObject

+(NSMutableArray *) getSksNewsByNet:(NSInteger *)beginNum setDepartId:(NSInteger *)departmentId;

@end
