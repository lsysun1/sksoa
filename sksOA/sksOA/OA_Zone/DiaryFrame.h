//
//  DiaryFrame.h
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class Diary;

@interface DiaryFrame : NSObject

@property (nonatomic,assign,readonly) CGRect aventorViewF;
@property (nonatomic,assign,readonly) CGRect nameLabelF;
@property (nonatomic,assign,readonly) CGRect timeLabelF;
@property (nonatomic,assign,readonly) CGRect contentLabelF;
@property (nonatomic,assign,readonly) CGRect firstImageViewF;
@property (nonatomic,assign,readonly) CGRect lineViewF;
@property (nonatomic,assign,readonly) CGRect cellViewF;

@property (nonatomic,assign,readonly) CGFloat cellHeight;

@property (nonatomic,copy)Diary *diary;

@end
