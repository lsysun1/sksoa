//
//  ImageCollectionViewCell.m
//  sksOA
//
//  Created by pandom on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //self.backgroundColor = [UIColor purpleColor];
        
        self.imageButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetWidth(self.frame))];
        [self addSubview:self.imageButtonView];
    }
    return self;
}



@end
