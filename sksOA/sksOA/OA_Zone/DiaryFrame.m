//
//  DiaryFrame.m
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#define JianJu 15
#define AventorJianJu 5
#define AventroHeight 40
#define NameWigth 80
#define NameHeight 25
#define TimeWigth 135
#define MaxContentHeight 90
#import "Diary.h"

#import "DiaryFrame.h"
@class Diary;
@implementation DiaryFrame

-(void)setDiary:(Diary *)diary
{
    _diary = diary;
    
    CGFloat _aventorX = AventorJianJu;
    CGFloat _aventorY = AventorJianJu;
    _aventorViewF = CGRectMake(_aventorX, _aventorY, AventroHeight, AventroHeight);
    
    CGFloat _nameLabelX = CGRectGetMaxX(_aventorViewF) + AventorJianJu;
    CGFloat _nameLabelY = _aventorY;
    _nameLabelF = CGRectMake(_nameLabelX, _nameLabelY, NameWigth, NameHeight);
    
    CGFloat _timeLabelX = CGRectGetMaxX(_nameLabelF)+JianJu;
    CGFloat _timeLabelY = _nameLabelY;
    _timeLabelF = CGRectMake(_timeLabelX, _timeLabelY,TimeWigth , NameHeight);
    
    CGFloat _contentX = _nameLabelX;
    CGFloat _contentY = CGRectGetMaxY(_nameLabelF) + JianJu;
    CGFloat _contentW = 320 - _nameLabelX -2*JianJu;
    NSStringDrawingOptions option = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12.0f] forKey:NSFontAttributeName];
    CGSize size = CGSizeMake(_contentW, MaxContentHeight);
    CGRect rect = [diary.content boundingRectWithSize:size options:option attributes:attributes context:nil];
    _contentLabelF = CGRectMake(_contentX, _contentY, _contentW, rect.size.height);
    
    
    CGFloat _cellViewHeight = CGRectGetMaxY(_contentLabelF) + JianJu;
    if ([diary.imageNumber intValue]>0) {
        CGFloat _imageViewX = _contentX;
        CGFloat _imageViewY = CGRectGetMaxY(_contentLabelF) + JianJu;
        CGFloat _imageViewW = _contentW;
        CGFloat _imageViewH = _contentW * 0.6;
        
        _firstImageViewF = CGRectMake(_imageViewX, _imageViewY, _imageViewW, _imageViewH);
        _cellViewHeight = CGRectGetMaxY(_firstImageViewF)+JianJu;
    }
    
    CGFloat _cellViewX = JianJu;
    CGFloat _cellViewY = JianJu;
    CGFloat _cellViewW = 320-2*JianJu;
    _cellViewF = CGRectMake(_cellViewX, _cellViewY,_cellViewW , _cellViewHeight);
    
    CGFloat lineViewX = 0;
    CGFloat lineViewY = CGRectGetMaxY(_cellViewF);
    _lineViewF = CGRectMake(lineViewX, lineViewY, 320, 1);
    
    _cellHeight = CGRectGetMaxY(_lineViewF);
    
}


@end
