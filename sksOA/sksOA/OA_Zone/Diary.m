//
//  Diary.m
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "Diary.h"

@implementation Diary

@synthesize diaryID,content,title,userID,username,date,imageNumber,imagePathes;

-(id)initWithDict:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.diaryID = [NSString stringWithFormat:@"%@",dict[@"diaryID"]];
        self.title = [NSString stringWithFormat:@"%@",dict[@"title"]];
        self.content = [NSString stringWithFormat:@"%@",dict[@"content"]];
        self.userID = [NSString stringWithFormat:@"%@",dict[@"userID"]];
        self.username = [NSString stringWithFormat:@"%@",dict[@"username"]];
        self.date = [[NSString stringWithFormat:@"%@",dict[@"date"]] substringToIndex:19];
        self.imageNumber = [NSString stringWithFormat:@"%@",dict[@"image_number"]];
    }
    return self;
}

+(id)diaryWithDict:(NSDictionary *)dict
{
    return [[Diary alloc]initWithDict:dict];
}


@end
