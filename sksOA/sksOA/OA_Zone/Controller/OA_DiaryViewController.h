//
//  OA_DiaryViewController.h
//  sksOA
//
//  Created by pandom on 15/5/13.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
@class Diary;

@interface OA_DiaryViewController : OA_BaseViewController

@property(copy,nonatomic) Diary *diary;

@end
