//
//  OA_ZoneViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ZoneViewController.h"
#import "Diary.h"
#import "DiaryFrame.h"
#import "DairyTableViewCell.h"
#import "MJRefresh.h"
#import "OA_DiaryViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "WriteDiaryViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"

#define UP 1
#define DOWN 0

@interface OA_ZoneViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_displayDiaryArray;
    NSMutableArray *_totalDiaryArray;
    NSString * _maxNumber;
    
}

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak,nonatomic) IBOutlet UIButton *writeBtn;

@end

@implementation OA_ZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_diaryTableView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [_writeBtn addTarget:self action:@selector(write) forControlEvents:UIControlEventTouchUpInside];
    [self.diaryTableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [self.diaryTableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadOldData)];
    
    _displayDiaryArray = [[NSMutableArray alloc]init];
    _totalDiaryArray = [[NSMutableArray alloc]init];
    [self sendRequestByNet:@"0" queryType:@"0" setLoadDirection:UP];
    
}

#pragma mark - 数据请求
- (void) sendRequestByNet:(NSString *) beginNum queryType:(NSString *)queryType setLoadDirection:(NSInteger) direction
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/zone";
    
    if (_displayDiaryArray.count >0 && direction==UP) {
        [_displayDiaryArray removeAllObjects];
        beginNum = @"0";
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"req_number"] = beginNum;
    parameters[@"query"] = queryType;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resArr = responseObject;
        Diary *diary = [[Diary alloc]init];
        for(NSDictionary *dict in resArr){
            DiaryFrame *frame = [[DiaryFrame alloc]init];
            diary = [Diary diaryWithDict:dict];
            
            //请求图片
            
            diary.imagePathes= [[NSMutableArray alloc]init];
            int count = [diary.imageNumber intValue];
            for(int i=0 ; i<count;i++)
            {
                NSString *imgUrl = [NSString stringWithFormat:@"http://182.140.244.136:8090/SKSAO/SKSimages/1/%@/%@_%d.png",diary.userID,diary.diaryID,1];
                [diary.imagePathes addObject:[NSURL URLWithString:imgUrl]];
            }
            
            frame.diary = diary;
            [_displayDiaryArray addObject:frame];
        }
        
        [_totalDiaryArray removeAllObjects];
        for (DiaryFrame *frame in _displayDiaryArray) {
            [_totalDiaryArray addObject:frame];
        }
        int count = (int)_totalDiaryArray.count;
        _maxNumber= [[_totalDiaryArray[count -1] diary] diaryID];
        
        [self.diaryTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _displayDiaryArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseID = @"diaryId";
    NSLog(@"diaryFrame numbers is %zi",_displayDiaryArray.count);
    NSLog(@"------%zi",indexPath.row);
    DairyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (cell == nil) {
        cell = [[DairyTableViewCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
    }
    
    if(_displayDiaryArray.count > 0){
        cell.diaryFrame = _displayDiaryArray[indexPath.row];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OA_DiaryViewController *diaryController = [[OA_DiaryViewController alloc]init];
    diaryController.diary = [_displayDiaryArray[indexPath.row] diary];
    [self.navigationController pushViewController:diaryController animated:YES];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0;
    if (_displayDiaryArray.count > 0) {
        height = [_displayDiaryArray[indexPath.row] cellHeight];
    }
    return height;
}

-(void)loadNewData
{
    [self.diaryTableView.header beginRefreshing];
    if (_maxNumber == nil) {
        _maxNumber = @"0";
    }
    [self sendRequestByNet:_maxNumber queryType:@"0" setLoadDirection:UP];
    [self.diaryTableView.header endRefreshing];
}

-(void)loadOldData
{
    [self.diaryTableView.footer beginRefreshing];
    if (_maxNumber == nil) {
        _maxNumber = @"0";
    }
    [self sendRequestByNet:_maxNumber queryType:@"0" setLoadDirection:DOWN];
    [self.diaryTableView.footer endRefreshing];
}

-(void)write
{
    WriteDiaryViewController *writeController = [[WriteDiaryViewController alloc]init];
    [self.navigationController pushViewController:writeController animated:YES];
}

- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
