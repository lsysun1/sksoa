//
//  OA_DiaryViewController.m
//  sksOA
//
//  Created by pandom on 15/5/13.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#define JianJu 15
#define AventorJianJu 5
#define AventroHeight 45
#define NameWigth 200
#define NameHeight 20
#define TimeWigth 135
#define MaxContentHeight 90

#import "Diary.h"
#import "DiaryFrame.h"
#import "OA_DiaryViewController.h"
#import "UIImageView+WebCache.h"
@class Diary;

@interface OA_DiaryViewController ()
{
    UIScrollView *_diaryScrollView;
    
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
    UIImageView *_aventor;
    NSMutableArray *_imageArray;
}

@property(weak,nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation OA_DiaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    _diaryScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-65)];
    
    _imageArray = [[NSMutableArray alloc]init];
    [self getImageArray:_diary];
    [_diaryScrollView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:2 alpha:0.1]];
    _diaryScrollView.contentSize = _diaryScrollView.frame.size;
    _diaryScrollView.showsVerticalScrollIndicator = YES;
    _diaryScrollView.showsHorizontalScrollIndicator = NO;
    _diaryScrollView.bounces = YES;
    [self.view addSubview:_diaryScrollView];
    [self setFrame:_diary];
    
}

-(void)setDiary:(Diary *)diary
{
    _diary = diary;
    
    _aventor = [[UIImageView alloc]init];
    _aventor.image = [UIImage imageNamed:@"zone.png"];
    
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.text = _diary.username;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    [_nameLabel setFont:[UIFont boldSystemFontOfSize:12.0f] ];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.text = _diary.date;
    _timeLabel.textAlignment = NSTextAlignmentLeft;
    [_timeLabel setFont:[UIFont systemFontOfSize:12.0f]];
    
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.text = _diary.content;
    _contentLabel.textAlignment = NSTextAlignmentLeft;
    [_contentLabel setFont:[UIFont systemFontOfSize:14.0f] ];
    _contentLabel.numberOfLines = 0;
    

}


-(void) setFrame:(Diary *)diary
{
    CGFloat _aventorX = AventorJianJu;
    CGFloat _aventorY = AventorJianJu;
    CGFloat _aventorW = AventroHeight;
    CGFloat _aventorH = AventroHeight;
    CGRect _aventorF = CGRectMake(_aventorX, _aventorY, _aventorW, _aventorH);
    _aventor.frame = _aventorF;
    [_diaryScrollView addSubview:_aventor];
    
    CGFloat _nameX = CGRectGetMaxX(_aventorF)+JianJu;
    CGFloat _nameY = _aventorY;
    CGRect _nameF = CGRectMake(_nameX, _nameY, NameWigth, NameHeight);
    _nameLabel.frame = _nameF;
    [_diaryScrollView addSubview:_nameLabel];
    
    CGFloat _timeX = _nameX;
    CGFloat _timeY = CGRectGetMaxY(_nameF)+AventorJianJu;
    CGRect _timeF = CGRectMake(_timeX, _timeY, NameWigth, NameHeight);
    _timeLabel.frame = _timeF;
    [_diaryScrollView addSubview:_timeLabel];
    
    
    CGFloat _contentX = JianJu;
    CGFloat _contentY = CGRectGetMaxY(_aventorF) + JianJu;
    CGFloat _contentW = 320 -2*JianJu;
    NSStringDrawingOptions option = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0f] forKey:NSFontAttributeName];
    CGSize size = CGSizeMake(_contentW, MAXFLOAT);
    CGRect rect = [diary.content boundingRectWithSize:size options:option attributes:attributes context:nil];
    _contentLabel.frame = CGRectMake(_contentX, _contentY, _contentW, rect.size.height);
    [_diaryScrollView addSubview:_contentLabel];
    
    CGFloat _imageX = JianJu;
    CGFloat tempHeight = CGRectGetMaxY(_contentLabel.frame)+JianJu;
    CGFloat _imageW = _contentW * 0.5;
    for (int i=0; i<[_diary.imageNumber intValue]; i++) {
        UIImageView *imageView = [[UIImageView alloc]init];
        [imageView sd_setImageWithURL:diary.imagePathes[i] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:SDWebImageCacheMemoryOnly];
        
        imageView.frame = CGRectMake(_imageX, tempHeight, _imageW,_imageW);
        [_diaryScrollView addSubview:imageView];
        tempHeight += (imageView.frame.size.height + AventorJianJu);
    }
    
    tempHeight += JianJu*3;
    if (tempHeight > _diaryScrollView.contentSize.height) {
        _diaryScrollView.contentSize = CGSizeMake(_diaryScrollView.contentSize.width ,tempHeight);
    }
}

-(void)getImageArray:(Diary *)diary
{

}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
