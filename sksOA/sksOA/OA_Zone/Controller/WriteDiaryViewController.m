//
//  WriteDiaryViewController.m
//  sksOA
//
//  Created by pandom on 15/5/14.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "WriteDiaryViewController.h"
#import "ImageCollectionViewCell.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFNetworking.h"
#import "AssetHelper.h"
#import "Diary.h"

@class Diary;

@interface WriteDiaryViewController ()<UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{ 
    NSMutableArray *_imageArray;
    NSString *_reuseID;
}
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollection;

@property(weak,nonatomic) IBOutlet UIButton *backBtn;
@property(weak,nonatomic) IBOutlet UIButton *sendBtn;
@end

@implementation WriteDiaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [_sendBtn addTarget:self action:@selector(sendDiaryToServer) forControlEvents:UIControlEventTouchUpInside];
    _contentTextView.delegate = self;
    _reuseID = @"CID";
    
    [_imageCollection setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
    [_imageCollection registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:_reuseID];
    _imageArray = [[NSMutableArray alloc]init];
    UIImage *image = [UIImage imageNamed:@"zoneuploadselectpic.png" ];
    [_imageArray addObject:image];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //隐藏键盘
    [_contentTextView resignFirstResponder];
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (![_contentTextView.text isEqualToString:@""]) {
        _placeHolderLabel.hidden = YES;
    }else{
        _placeHolderLabel.hidden = NO;
    }
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imageArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ImageCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_reuseID forIndexPath:indexPath];
   
    if (cell ==nil) {
        NSLog(@"ImageCollectionViewCell 注册失败！");
    }
    [cell.imageButtonView setImage:_imageArray[indexPath.row] forState:UIControlStateNormal] ;
    [cell.imageButtonView removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    if (indexPath.row == _imageArray.count-1) {
        [cell.imageButtonView addTarget:self action:@selector(onShowImagePicker) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void)onShowImagePicker
{
    
    DoImagePickerController *cont = [[DoImagePickerController alloc] initWithNibName:@"DoImagePickerController" bundle:nil];
    cont.delegate = self;
    cont.nResultType = DO_PICKER_RESULT_UIIMAGE;
    
    cont.nMaxCount = DO_NO_LIMIT_SELECT;
    cont.nResultType = DO_PICKER_RESULT_ASSET;  // if you want to get lots photos, you'd better use this mode for memory!!!
    
    
    cont.nColumnCount = 3;
    
    [self presentViewController:cont animated:YES completion:nil];
}

-(void)didCancelDoImagePickerController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didSelectPhotosFromDoImagePickerController:(DoImagePickerController *)picker result:(NSArray *)aSelected
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [_imageArray removeAllObjects];
    if (picker.nResultType == DO_PICKER_RESULT_UIIMAGE)
    {
        for (int i = 0; i < aSelected.count; i++)
        {
            [_imageArray addObject:aSelected[i]];
        }
    }
    else if (picker.nResultType == DO_PICKER_RESULT_ASSET)
    {
        for (int i = 0; i < aSelected.count; i++)
        {
            [_imageArray addObject:[ASSETHELPER getImageFromAsset:aSelected[i] type:ASSET_PHOTO_SCREEN_SIZE]];
        }
        
        [ASSETHELPER clearData];
    }
    UIImage *image = [UIImage imageNamed:@"zoneuploadselectpic.png" ];
    [_imageArray addObject:image];
    [self.imageCollection reloadData];
}

-(void)sendDiaryToServer
{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefaultes objectForKey:@"userID"];
    NSLog(@"userID = %@",userID);
    
    NSString *content = _contentTextView.text;
    NSLog(@"content = %@" ,content);
    
    NSString * imageNum = [NSString stringWithFormat:@"%zi",_imageArray.count-1 ];
    NSLog(@"imageNum = %@" ,imageNum);
    
    NSMutableDictionary *para = [[NSMutableDictionary alloc]init];
    [para setValue:userID forKey:@"userID"];
    [para setValue:content forKey:@"content"];
    [para setValue:imageNum forKey:@"imageNumber"];
    [self postDiaryToServer:para];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(NSString *)postDiaryToServer:(NSMutableDictionary *)para
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/zone";
   
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    __block NSString * result = nil;
    [manager POST:url parameters:para success:^(AFHTTPRequestOperation *operation, id responseObject) {
        result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"res -- %@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    return result;
}


// 上传图片
- (void)uploadImageWithImage:(UIImage *)image setDiaryID:(NSString *)dID setSeq_Num:(NSString *)seq

{
    //上传其他所需参数
    NSString *userID= [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"];
    NSString *imageName = [NSString stringWithFormat:@"%@-%@-%@",userID,dID,seq];
    
    //上传请求POST
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:imageName forKey:@"filename"];
    
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
