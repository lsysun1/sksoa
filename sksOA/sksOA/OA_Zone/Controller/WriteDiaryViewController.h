//
//  WriteDiaryViewController.h
//  sksOA
//
//  Created by pandom on 15/5/14.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
#import "DoImagePickerController.h"

@interface WriteDiaryViewController : OA_BaseViewController<DoImagePickerControllerDelegate>

@end
