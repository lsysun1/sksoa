//
//  ImageCollectionViewCell.h
//  sksOA
//
//  Created by pandom on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property(strong,nonatomic ) UIButton *imageButtonView;

@end
