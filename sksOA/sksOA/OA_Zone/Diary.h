//
//  Diary.h
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Diary : NSObject

@property(copy,nonatomic) NSString * diaryID;
@property(copy,nonatomic) NSString * userID;
@property(copy,nonatomic) NSString * content;
@property(copy,nonatomic) NSString * title;
@property(copy,nonatomic) NSString * date;
@property(copy,nonatomic) NSString * username;
@property(copy,nonatomic) NSString * imageNumber;
@property(retain,nonatomic) NSMutableArray *imagePathes;

-(id)initWithDict:(NSDictionary *) dict;
+(id)diaryWithDict:(NSDictionary *) dict;
@end
