//
//  DairyTableViewCell.h
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiaryFrame.h"

@interface DairyTableViewCell : UITableViewCell

@property (nonatomic,strong)DiaryFrame *diaryFrame;

@end
