//
//  DairyTableViewCell.m
//  sksOA
//
//  Created by pandom on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "DairyTableViewCell.h"
#import "Diary.h"
#import "UIImageView+WebCache.h"
@class Diary;

@interface DairyTableViewCell()
{
    UIImageView *_aventorView;
    UILabel *_namelabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
    UIImageView *_firstImageView;
    UIView *_lineView;
    UIView *_cellView;

}
@end

@implementation DairyTableViewCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        _cellView = [[UIView alloc]init];
        [_cellView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:2 alpha:0.1]];
        [self.contentView addSubview:_cellView];
        
        _aventorView = [[UIImageView alloc]init];
        [_cellView addSubview:_aventorView];
        
        _namelabel = [[UILabel alloc]init];
        _namelabel.font = [UIFont systemFontOfSize:12.0f];
        _namelabel.textAlignment = NSTextAlignmentLeft;
        _namelabel.textColor = [UIColor blackColor];
        [_cellView addSubview:_namelabel];
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:8.0f];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.textColor = [UIColor blackColor];
        [_cellView addSubview:_timeLabel];
        
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font = [UIFont systemFontOfSize:12.0f];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor blackColor];
        [_cellView addSubview:_contentLabel];
        
        _firstImageView = [[UIImageView alloc]init];
        [_cellView addSubview:_firstImageView];
        
       
        _lineView = [[UIView alloc]init];
        [_lineView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:2 alpha:0.1]];
        [self.contentView addSubview:_lineView];
    }
    return self;
}


-(void)setDiaryFrame:(DiaryFrame *)diaryFrame
{
    _diaryFrame = diaryFrame;
    
    [self settingSubViewFrame:diaryFrame];
    
    [self settingData];

}

-(void)settingSubViewFrame:(DiaryFrame *)diaryFrame
{
    _aventorView.frame = diaryFrame.aventorViewF;
    
    _namelabel.frame = diaryFrame.nameLabelF;
    
    _contentLabel.frame = diaryFrame.contentLabelF;
    
    _timeLabel.frame = diaryFrame.timeLabelF;
    
    _firstImageView.frame = diaryFrame.firstImageViewF;
    
    _lineView.frame = diaryFrame.lineViewF;
    
    _cellView.frame = diaryFrame.cellViewF;
}

-(void)settingData
{
    Diary *diary = _diaryFrame.diary;
    
    _aventorView.image = [UIImage imageNamed:@"zone.png"];
    
    _namelabel.text = diary.username;
    
    _timeLabel.text = diary.date;
    
    _contentLabel.text = diary.content;
    
    if ([diary.imageNumber intValue]>0) {
        _firstImageView.hidden = NO;
        [_firstImageView sd_setImageWithURL:diary.imagePathes[0] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:SDWebImageCacheMemoryOnly];
    }else{
        _firstImageView.hidden = YES;
    }
    

}


@end
