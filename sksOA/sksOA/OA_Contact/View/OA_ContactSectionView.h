//
//  OA_ContactSectionView.h
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OA_ContactSectionView,OA_PartModel,OA_GroupModel;

@protocol OA_SectionViewDelegate <NSObject>

- (void) myHeadViewDicClick:(OA_ContactSectionView *) sectionView;

@end


@interface OA_ContactSectionView : UITableViewHeaderFooterView

+ (instancetype) sectionViewWithTableView:(UITableView *)tableView;

@property (nonatomic ,strong) OA_GroupModel *group;
@property (nonatomic,weak) id<OA_SectionViewDelegate> delegate;



@end
