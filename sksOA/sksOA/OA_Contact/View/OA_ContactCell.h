//
//  OA_ContactCell.h
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OA_PersonModel;

@protocol ContactCellDelegate <NSObject>

//- (void) callNumMethod:(nss *)btn;

@end


@interface OA_ContactCell : UITableViewCell

@property (nonatomic, strong) OA_PersonModel *person;
@property (nonatomic, assign) id <ContactCellDelegate> delegate;

+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
