//
//  OA_ContactCell.m
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ContactCell.h"
#import "OA_PersonModel.h"


@interface OA_ContactCell()
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *mobilPhone;
@property (strong, nonatomic) IBOutlet UILabel *phone;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *mobilphoneLine;
@property (strong, nonatomic) IBOutlet UILabel *phoneLine;

@property (strong, nonatomic) IBOutlet UIButton *mobilPhoneBtn;
@property (strong, nonatomic) IBOutlet UIButton *phoneBtn;

@end


@implementation OA_ContactCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"Contact_Cell";
    OA_ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"OA_ContactCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    }
    return cell;
}




- (void)awakeFromNib {
    // Initialization code
    self.mobilPhoneBtn.tag = 999;
    self.phoneBtn.tag = 666;
    [self.mobilPhoneBtn addTarget:self action:@selector(mobilPhoneBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.phoneBtn addTarget:self action:@selector(phoneBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
}



- (void) setPerson:(OA_PersonModel *)person
{
    self.userName.text = person.username;

    if(![person.mobilePhone isEqualToString:@"<null>"])
    {
        self.mobilPhone.text = person.mobilePhone;
        self.mobilphoneLine.hidden = NO;
    }
    
    if(![person.familyPhone isEqualToString:@"<null>"])
    {
        self.phone.text = person.familyPhone;
        self.phoneLine.hidden = NO;
    }
    
    if(![person.position isEqualToString:@"<null>"])
    {
        self.title.text = person.position;
    }
    


}


#pragma mark - 图片处理
- (UIImage *)ImageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (void) mobilPhoneBtnDidClick
{
    if(self.mobilPhone.text != nil){
        NSString *url = self.mobilPhone.text;
        if (url.length > 1) {
            NSString *telUrl = [NSString stringWithFormat:@"telprompt:%@",url];
            NSURL *url = [[NSURL alloc] initWithString:telUrl];
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}
    
- (void) phoneBtnDidClick
{
    if(self.phone.text != nil){
        NSString *url = self.phone.text;
        if (url.length > 1) {
            NSString *telUrl = [NSString stringWithFormat:@"telprompt:%@",url];
            NSURL *url = [[NSURL alloc] initWithString:telUrl];
            [[UIApplication sharedApplication] openURL:url];
        }
        
    }

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
