//
//  OA_ContactSectionView.m
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ContactSectionView.h"
#import "OA_GroupModel.h"

@interface OA_ContactSectionView()
@property(nonatomic,strong) UIButton *nameButton;

@end

@implementation OA_ContactSectionView

+ (instancetype) sectionViewWithTableView:(UITableView *)tableView
{
    //  1.创建头部View
    static NSString *ID = @"headView";
    OA_ContactSectionView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(headView == nil){
        headView = [[OA_ContactSectionView alloc]initWithReuseIdentifier:ID];
        
    }
    
    return headView;
}


- (id) initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if(self == [super initWithReuseIdentifier:reuseIdentifier]){
        //  添加子控件
        
        //  1.添加按钮(显示名字)
        UIButton *nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //  1.1 设置按钮的背景图片
        
//        UIImage *originImg = [self ImageFromColor:UIColorFromRGB(0x578087)];
//        
//        [nameButton setBackgroundImage:originImg forState:UIControlStateNormal];
//        [nameButton setBackgroundImage:originImg forState:UIControlStateHighlighted];
        //  1.2 设置按钮的箭头图片和内容模式
        nameButton.backgroundColor = [UIColor clearColor];
        [nameButton setImage:[UIImage imageNamed:@"indicator0"] forState:UIControlStateNormal];
        [nameButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        nameButton.imageView.contentMode = UIViewContentModeCenter;
        nameButton.imageView.clipsToBounds = NO;
        
        //  1.3 设置按钮的内容左对齐
        nameButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //  1.4 设置按钮的整体边距
        nameButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        //  1.5 设置按钮的title左边距
        nameButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        //  1.6 给按钮添加一个方法用来监听点击时间
        [nameButton addTarget:self action:@selector(nameButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        self.nameButton = nameButton;
        [self.contentView addSubview:nameButton];
  
    }
    return self;
}

/**
 *  当一个控件的frame发生改变的时候就会调用这个方法
 *  一般在这里设置内部子控件的frame
 */
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //  1.设置nameButton的Frame
    self.nameButton.frame = self.bounds;
    
}


- (void) setGroup:(OA_GroupModel *)group
{
    _group = group;
    
    //  1.设置按钮的文字
    [self.nameButton setTitle:group.name forState:UIControlStateNormal];
    
    
    //  3.重置左边的箭头图案
    [self didMoveToSuperview];

}





/**
 *  监听组名按钮的点击事件
 */
- (void) nameButtonClick
{
    //  1.修改模型 (取反)
    self.group.open = !self.group.isOpen;
    
    //  2.刷新表格
    //  如果代理有实现代理方法,就把自己传给代理
    if([self.delegate respondsToSelector:@selector(myHeadViewDicClick:)]){
        [self.delegate myHeadViewDicClick:self];
    }
}


/**
 *  当一个控件被添加到父控件中的时候,会调用这个方法
 */
- (void) didMoveToSuperview
{
    if(self.group.isOpen){
        self.nameButton.imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }else{
        self.nameButton.imageView.transform = CGAffineTransformMakeRotation(0);
    }
    
}



#pragma mark - 图片处理
- (UIImage *)ImageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}






@end
