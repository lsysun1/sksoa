//
//  OA_ContactViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ContactViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "OA_PartModel.h"
#import "OA_PersonModel.h"
#import "OA_ContactCell.h"

@interface OA_ContactViewController ()<UISearchResultsUpdating,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) NSMutableArray *depArr;
@property (strong, nonatomic) NSMutableArray *vcArr;
@property (strong, nonatomic) NSMutableArray *personArr;
@property (strong, nonatomic) NSMutableArray *allSubDepArr;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;

@property (nonatomic, retain) NSMutableArray *searchResultDataArray;            // 存放搜索出结果的数组
@property (nonatomic, retain) UISearchController *searchController;             // 搜索控制器
@property (nonatomic, retain) UITableViewController *searchTVC;                 // 搜索使用的表示图控制器


@end

@implementation OA_ContactViewController
@synthesize listVCArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _depArr = [[NSMutableArray alloc]init];
        _vcArr = [[NSMutableArray alloc]init];
        _personArr = [[NSMutableArray alloc]init];
        _allSubDepArr = [[NSMutableArray alloc]init];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.searchBtn addTarget:self action:@selector(searchBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.searchView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    self.topView.backgroundColor = UIColorFromRGB(0x552b3f);
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.title = @"滑动切换视图";
    self.slideSwitchView.tabItemNormalColor = [UIColor whiteColor];
    self.slideSwitchView.tabItemSelectedColor = [UIColor whiteColor];
    self.slideSwitchView.shadowImage = [[UIImage imageNamed:@"tab_line"]
                                        stretchableImageWithLeftCapWidth:59.0f topCapHeight:0.0f];
    
    

    
    [self sendRequestForDepartment];
    
}


- (void) searchBtnDidClick
{
    // 创建出搜索使用的表示图控制器
    self.searchTVC = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _searchTVC.tableView.dataSource = self;
    _searchTVC.tableView.delegate = self;

    
    
    // 使用表示图控制器创建出搜索控制器
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:_searchTVC];
    
    // 搜索框检测代理
    //（这个需要遵守的协议是 <UISearchResultsUpdating> ，这个协议中只有一个方法，当搜索框中的值发生变化的时候，代理方法就会被调用）
    _searchController.searchResultsUpdater = self;
    _searchController.searchBar.placeholder = @"姓名 | 电话 ";
    _searchController.searchBar.prompt = @"输入要查找的同事";
    
    // 因为搜索是控制器，所以要使用模态推出（必须是模态，不可是push）
    [self presentViewController:_searchController animated:YES completion:nil];


}


#pragma mark _tableViewDelegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResultDataArray.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OA_ContactCell *cell = [OA_ContactCell cellWithTableView:tableView];
    
    OA_PersonModel *person = self.searchResultDataArray[indexPath.row];
    
    cell.person = person;
    
    UIImage *originImg = [self ImageFromColor:[UIColor blackColor]];
    UIImage *bgImg = [self imageByApplyingAlpha:0.4 image:originImg];
    
    cell.backgroundView = [[UIImageView alloc]initWithImage:bgImg];
    
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}



#pragma mark - 请求部门数量
- (void) sendRequestForDepartment
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/contact";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"0";
    parameters[@"contact_department_update"] = @"2015-5-11";
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resArr = responseObject;
        for(NSDictionary *dict in resArr){
            OA_PartModel *part = [OA_PartModel modelWithDict:dict];
            if([part.parentDepartment isEqualToString:@"<null>"]){
                [self.depArr addObject:part];
            }
        }
        
        for(NSDictionary *dict in resArr){
            OA_PartModel *part = [OA_PartModel modelWithDict:dict];
            if(![part.parentDepartment isEqualToString:@"<null>"]){
                [self.allSubDepArr addObject:part];
            }
        }
        
        
        
        [self sendRequsetForAllPerson];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
}

#pragma mark - UISearchResultsUpdating Method
#pragma mark 监听者搜索框中的值的变化
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    // 1. 获取输入的值
    NSString *conditionStr = searchController.searchBar.text;
    
    // 2. 创建谓词，准备进行判断的工具
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.mobilePhone CONTAINS [CD] %@ OR self.username CONTAINS [CD] %@ OR self.familyPhone CONTAINS [CD] %@", conditionStr, conditionStr, conditionStr];
    
    // 3. 使用工具获取匹配出的结果
    self.searchResultDataArray = [NSMutableArray arrayWithArray:[self.personArr filteredArrayUsingPredicate:predicate]];
    
    // 4. 刷新页面，将结果显示出来
    [_searchTVC.tableView reloadData];
}



- (void) sendRequsetForAllPerson
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/contact";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"1";
    parameters[@"contact_user_update"] = @"2015-5-11";
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resArr = responseObject;
        for(NSDictionary *dict in resArr){
            OA_PersonModel *person = [OA_PersonModel modelWithDict:dict];
            [self.personArr addObject:person];
        }
        
        [self creatListViewController];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];


}




#pragma mark - 滑动tab视图代理方法

- (void) creatListViewController
{
    for(int i = 0;i<_depArr.count;i++){
        OA_ContactListViewController *vc = [[OA_ContactListViewController alloc]init];
        vc.allPersonArr = self.personArr;
        vc.allSubDepArr = self.allSubDepArr;
        OA_PartModel *model = _depArr[i];
        vc.partModel = model;
        vc.title = model.departmentName;
        [self.vcArr addObject:vc];
    }
    
    self.listVCArray = self.vcArr;
    
    
    
    [self.slideSwitchView buildUI];


}



- (NSUInteger)numberOfTab:(OA_SlideSwitchView *)view
{
    // you can set the best you can do it ;
    return self.listVCArray.count;
}

- (OA_ContactListViewController *)slideSwitchView:(OA_SlideSwitchView *)view viewOfTab:(NSUInteger)number
{
    return self.listVCArray[number];

}


- (void)slideSwitchView:(OA_SlideSwitchView *)view didselectTab:(NSUInteger)number
{
    OA_ContactListViewController *vc = nil;
    vc = self.listVCArray[number];
    [vc viewDidCurrentView];
}




- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - 内存报警

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
