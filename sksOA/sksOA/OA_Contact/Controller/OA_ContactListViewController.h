//
//  OA_ContactListViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
@class OA_PartModel;
@interface OA_ContactListViewController : OA_BaseViewController
@property (nonatomic, strong) OA_PartModel *partModel;
@property (nonatomic, strong) NSArray *allPersonArr;
@property (nonatomic, strong) NSArray *allSubDepArr;
- (void)viewDidCurrentView;
@end
