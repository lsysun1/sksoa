//
//  OA_ContactViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "OA_BaseViewController.h"
#import "OA_ContactListViewController.h"
#import "OA_SlideSwitchView.h"


@interface OA_ContactViewController : OA_BaseViewController<OA_SlideSwitchViewDelegate>
{
    OA_SlideSwitchView *_slideSwitchView;
    NSMutableArray *listVCArray;

}

@property (nonatomic, strong) IBOutlet OA_SlideSwitchView *slideSwitchView;

@property (nonatomic, strong) NSMutableArray *listVCArray;


@end

