//
//  OA_ContactListViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ContactListViewController.h"
#import "OA_PartModel.h"
#import "AFHTTPRequestOperationManager.h"
#import "OA_GroupModel.h"
#import "OA_PersonModel.h"
#import "OA_ContactSectionView.h"
#import "OA_ContactCell.h"

@interface OA_ContactListViewController () <UITableViewDataSource,UITableViewDelegate,OA_SectionViewDelegate,ContactCellDelegate>
{
    NSMutableArray *_groupNumArr;
    NSMutableArray *_subDepArr;
    UITableView *_tableView;
    NSMutableArray *_groupArr;
}
@end

@implementation OA_ContactListViewController

- (void)viewDidLoad {
    
    _groupNumArr = [[NSMutableArray alloc]init];
    _groupArr = [[NSMutableArray alloc]init];
    _subDepArr = [[NSMutableArray alloc]init];
    
    [super viewDidLoad];
    [self setUpTableView];

    
}


- (void)viewDidCurrentView
{
    NSLog(@"加载为当前视图 = %@",self.title);
    [self sendRequestForDepartment];
}






#pragma mark - 请求网络数据
- (void) sendRequestForDepartment
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/contact";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"0";
    parameters[@"contact_department_update"] = @"2015-5-11";
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [_groupNumArr removeAllObjects];
        NSArray *resArr = responseObject;
        for(NSDictionary *dict in resArr)
        {
            OA_GroupModel *group = [OA_GroupModel modelWithDict:dict];
            if([group.parentDepartment isEqualToString:self.title]){
                [_groupNumArr addObject:group];
            }
        }
        
        
        [_subDepArr removeAllObjects];
        
        [_groupArr removeAllObjects];
        for(OA_PartModel *subDep in self.allSubDepArr){
            if([subDep.parentDepartment isEqualToString:self.title]){
                OA_GroupModel *group = [[OA_GroupModel alloc]init];
                group.name = subDep.departmentName;
                group.personsArr = [[NSMutableArray alloc]init];
                
                for(OA_PersonModel *person in self.allPersonArr){
                    if([person.company isEqualToString:subDep.departmentName])
                       {
                           group.name = person.company;
                           [group.personsArr addObject:person];
                       }

                }
                
                [_groupArr addObject:group];
                
                [_subDepArr addObject:group];
            }
        }
        
        
        
        for(int i = 0;i<self.allPersonArr.count;i++){
            OA_PersonModel *person = self.allPersonArr[i];
//            NSLog(@"person -- conpany = %@",person.company);
            if([person.company isEqualToString:self.title]){
            }
        }
        
        [_tableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];

}



#pragma mark - 创建_tableView
- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 420)];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.sectionHeaderHeight = 30;
    _tableView.separatorStyle = 0;
    _tableView.rowHeight = 80.0f;
    _tableView.sectionIndexBackgroundColor = [UIColor redColor];
    [self.view addSubview:_tableView];

}

#pragma mark _tableViewDelegate Method
/**
 *  一共有多少组
 */
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return _groupArr.count;
}

/**
 *  每一组有多少行数据
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //  首先取出
    OA_GroupModel *groups = _groupArr[section];
    
    return (groups.isOpen ? groups.personsArr.count : 0);}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OA_ContactCell *cell = [OA_ContactCell cellWithTableView:tableView];
    cell.delegate = self;
    
    OA_GroupModel *groups = _groupArr[indexPath.section];
    OA_PersonModel *person = groups.personsArr[indexPath.row];
    
    cell.person = person;
    
    UIImage *originImg = [self ImageFromColor:[UIColor blackColor]];
    UIImage *bgImg = [self imageByApplyingAlpha:0.4 image:originImg];
    
    cell.backgroundView = [[UIImageView alloc]initWithImage:bgImg];

    
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //  这个HeaderView跟Cell一样有循环利用机制
    OA_ContactSectionView *sectionView = [OA_ContactSectionView sectionViewWithTableView:tableView];
    sectionView.delegate = self;
    
    UIImage *originImg = [self ImageFromColor:UIColorFromRGB(0x552b3f)];
    UIImage *bgImg = [self imageByApplyingAlpha:0.5 image:originImg];
    
    sectionView.backgroundView = [[UIImageView alloc]initWithImage:bgImg];
    
    //  给HeaderView传递模型
    sectionView.group = _groupArr[section];
    
    return sectionView;
}




#pragma mark - MyHeadViewDelegate Method
- (void) myHeadViewDicClick:(OA_ContactSectionView *)headView
{
    [_tableView reloadData];
}

//#pragma mark - OA_ContactCell Delegate
//- (void) callNumMethod:(UIButton *)btn
//{
//    NSLog(@"btn.num -- %@",btn.titleLabel.text);
//
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
