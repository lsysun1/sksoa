//
//  OA_GroupModel.h
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OA_GroupModel : NSObject

@property(copy,nonatomic) NSString *departmentName;
@property(copy,nonatomic) NSString *parentDepartment;
@property(copy,nonatomic) NSString *name;


/**
 *  这个数组中存放OA_PersonModel模型
 */
@property(strong,nonatomic) NSMutableArray*personsArr;

@property(nonatomic,assign, getter=isOpen) BOOL open;


-(id)initWithDict:(NSDictionary *) dict;
+(id)modelWithDict:(NSDictionary *) dict;


@end
