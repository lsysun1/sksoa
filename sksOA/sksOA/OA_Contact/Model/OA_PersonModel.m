//
//  OA_PersonModel.m
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_PersonModel.h"

@implementation OA_PersonModel

+(id)modelWithDict:(NSDictionary *)dict
{
    return [[OA_PersonModel alloc]initWithDict:dict];
}

-(id)initWithDict:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.position = [NSString stringWithFormat:@"%@",dict[@"position"]];
        self.userID = [NSString stringWithFormat:@"%@",dict[@"userID"]];
        self.username = [NSString stringWithFormat:@"%@",dict[@"username"]];
        self.otherContact = [NSString stringWithFormat:@"%@",dict[@"otherContact"]];
        self.mobilePhone = [NSString stringWithFormat:@"%@",dict[@"mobilePhone"]];
        self.remark = [NSString stringWithFormat:@"%@",dict[@"remark"]];
        self.email = [NSString stringWithFormat:@"%@",dict[@"email"]];
        self.address = [NSString stringWithFormat:@"%@",dict[@"address"]];
        self.zipCode = [NSString stringWithFormat:@"%@",dict[@"zipCode"]];
        self.company = [NSString stringWithFormat:@"%@",dict[@"company"]];
        self.familyPhone = [NSString stringWithFormat:@"%@",dict[@"familyPhone"]];
        self.officePhone = [NSString stringWithFormat:@"%@",dict[@"officePhone"]];
    }
    return self;
}



@end
