//
//  OA_PartModel.h
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OA_PartModel : NSObject

@property(copy,nonatomic) NSString *departmentName;
@property(copy,nonatomic) NSString *parentDepartment;


-(id)initWithDict:(NSDictionary *) dict;
+(id)modelWithDict:(NSDictionary *) dict;

@end
