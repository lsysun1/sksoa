//
//  OA_GroupModel.m
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_GroupModel.h"

@implementation OA_GroupModel


+(id)modelWithDict:(NSDictionary *)dict
{
    return [[OA_GroupModel alloc]initWithDict:dict];
}

-(id)initWithDict:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.departmentName = [NSString stringWithFormat:@"%@",dict[@"departmentName"]];
        self.parentDepartment = [NSString stringWithFormat:@"%@",dict[@"parentDepartment"]];
    }
    return self;
}




@end
