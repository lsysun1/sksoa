//
//  OA_PersonModel.h
//  sksOA
//
//  Created by 李松玉 on 15/5/12.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OA_PersonModel : NSObject

@property(copy,nonatomic) NSString *position;
@property(copy,nonatomic) NSString *userID;
@property(copy,nonatomic) NSString *username;
@property(copy,nonatomic) NSString *otherContact;
@property(copy,nonatomic) NSString *mobilePhone;
@property(copy,nonatomic) NSString *remark;
@property(copy,nonatomic) NSString *email;
@property(copy,nonatomic) NSString *address;
@property(copy,nonatomic) NSString *zipCode;
@property(copy,nonatomic) NSString *company;
@property(copy,nonatomic) NSString *familyPhone;
@property(copy,nonatomic) NSString *officePhone;

-(id)initWithDict:(NSDictionary *) dict;
+(id)modelWithDict:(NSDictionary *) dict;




@end



