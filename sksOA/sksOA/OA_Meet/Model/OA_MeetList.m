//
//  OA_MeetList.m
//  sksOA
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_MeetList.h"

@implementation OA_MeetList

+(id)modelWithDict:(NSDictionary *)dict
{
    return [[OA_MeetList alloc]initWithDict:dict];
}

-(id)initWithDict:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.responsor = [NSString stringWithFormat:@"%@",dict[@"responsor"]];
        self.roomName = [NSString stringWithFormat:@"%@",dict[@"roomName"]];
        self.capacity = [NSString stringWithFormat:@"%@",dict[@"capacity"]];
        self.equipment = [NSString stringWithFormat:@"%@",dict[@"equipment"]];
        self.roomID = [NSString stringWithFormat:@"%@",dict[@"roomID"]];
    }
    return self;
}

@end
