//
//  OA_MeetList.h
//  sksOA
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OA_MeetList : NSObject

@property(copy,nonatomic) NSString *responsor;
@property(copy,nonatomic) NSString *roomName;
@property(copy,nonatomic) NSString *capacity;
@property(copy,nonatomic) NSString *equipment;
@property(copy,nonatomic) NSString *roomID;



-(id)initWithDict:(NSDictionary *) dict;
+(id)modelWithDict:(NSDictionary *) dict;



@end
