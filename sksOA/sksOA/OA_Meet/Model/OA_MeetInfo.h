//
//  OA_MeetInfo.h
//  sksOA
//
//  Created by 李松玉 on 15/5/18.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OA_MeetInfo : NSObject

@property(copy,nonatomic) NSString *content;
@property(copy,nonatomic) NSString *day;
@property(copy,nonatomic) NSString *endTime;
@property(copy,nonatomic) NSString *startTime;
@property(copy,nonatomic) NSString *theme;
@property(copy,nonatomic) NSString *uid;



-(id)initWithDict:(NSDictionary *) dict;
+(id)modelWithDict:(NSDictionary *) dict;

@end
