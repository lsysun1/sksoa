//
//  OA_MeetInfo.m
//  sksOA
//
//  Created by 李松玉 on 15/5/18.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_MeetInfo.h"

@implementation OA_MeetInfo

+(id)modelWithDict:(NSDictionary *) dict
{
    return [[self alloc]initWithDict:dict];
}

-(id)initWithDict:(NSDictionary *) dict
{
    self = [super init];
    if(self){
        self.content = [NSString stringWithFormat:@"%@",dict[@"content"]];
        self.day = [NSString stringWithFormat:@"%@",dict[@"day"]];
        self.endTime = [NSString stringWithFormat:@"%@",dict[@"endTime"]];
        self.startTime = [NSString stringWithFormat:@"%@",dict[@"startTime"]];
        self.theme = [NSString stringWithFormat:@"%@",dict[@"theme"]];
        self.uid = [NSString stringWithFormat:@"%@",dict[@"uid"]];
    }
    return self;
}


@end
