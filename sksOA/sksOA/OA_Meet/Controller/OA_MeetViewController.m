//
//  OA_MeetViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_MeetViewController.h"
#import "OA_OrderRoomViewController.h"
#import "OA_MeetCell.h"
#import "OA_MeetList.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFURLResponseSerialization.h"


@interface OA_MeetViewController () <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *meetListArr;
@end

@implementation OA_MeetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.meetListArr = [[NSMutableArray alloc]init];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self setUpTableView];
    [self sendRequsetForMeetList];
}


- (void) setUpTableView
{
    self.tableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 60, ScreenWidth, ScreenHeight - 60)];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = 251;
    self.tableView.separatorStyle = 0;
    [self.view addSubview:self.tableView];
}

- (void) sendRequsetForMeetList
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/MeetingServ";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"2";
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resArr = responseObject;
        for(NSDictionary *dict in resArr){
            OA_MeetList *room = [OA_MeetList objectWithKeyValues:dict];
            [self.meetListArr addObject:room];
        }
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.meetListArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OA_MeetCell *cell = [OA_MeetCell cellWithTableView:tableView];
    OA_MeetList *model = self.meetListArr[indexPath.row];
    cell.meetList = model;
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OA_MeetList *model = self.meetListArr[indexPath.row];

    OA_OrderRoomViewController *orderVC = [[OA_OrderRoomViewController alloc]init];
    orderVC.model = model;
    
    [self.navigationController pushViewController:orderVC animated:YES];


}




- (void) roomBtnDidClick:(UIButton *)btn
{
    OA_OrderRoomViewController *orderVC = [[OA_OrderRoomViewController alloc]init];
    [self.navigationController pushViewController:orderVC animated:YES];
}



- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
