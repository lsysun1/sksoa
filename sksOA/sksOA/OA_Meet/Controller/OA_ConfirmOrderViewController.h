//
//  OA_ConfirmOrderViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
@class OA_MeetList;
@interface OA_ConfirmOrderViewController : OA_BaseViewController
@property (nonatomic, copy) NSString *meetDateStr;
@property (nonatomic, copy) NSString *meetTimeStr;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic ,strong) OA_MeetList *model;

@end
