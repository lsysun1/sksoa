//
//  OA_ConfirmOrderViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_ConfirmOrderViewController.h"
#import "OA_MeetList.h"


@interface OA_ConfirmOrderViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *midView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;

@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIDatePicker *startPicker;
@property (strong, nonatomic) UIDatePicker *endPicker;

@property (strong, nonatomic) IBOutlet UITextField *theme;
@property (strong, nonatomic) IBOutlet UITextField *content;

@property (strong, nonatomic) IBOutlet UIButton *submitBtn;




@end

@implementation OA_ConfirmOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.theme.delegate = self;
    self.content.delegate = self;
    
    [self.submitBtn addTarget:self action:@selector(submitBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.dateBtn addTarget:self action:@selector(dateBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.startBtn addTarget:self action:@selector(startBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.endBtn addTarget:self action:@selector(endBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    self.dateLabel.text = self.meetDateStr;
    self.timeLabel.text = self.meetTimeStr;
    
    
}


- (void) submitBtnDidClick
{
    [self sendRequestForOrderRoom];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //使textField取消第一响应者，从而隐藏键盘
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.datePicker removeFromSuperview];
    [self.startPicker removeFromSuperview];
    [self.endPicker removeFromSuperview];

    [self.view endEditing:YES];
}


- (void) dateBtnDidClick
{
    [self pickerViewIsOnView];

    self.datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, ScreenHeight - 200, ScreenWidth, 200)];
    [self.datePicker setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_Hans_CN"]];

    if(self.dateLabel.text != nil){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        NSDate *date = [dateFormatter dateFromString:self.dateLabel.text];
        [self.datePicker setDate:date animated:YES];
        
    }else{
        [self.datePicker setDate:self.date animated:YES];

    }
    
    [self.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.datePicker];
    
}

- (void) datePickerValueChanged:(UIDatePicker *)picker
{
    NSDate *select = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    self.dateLabel.text = dateAndTime;
}



- (void) startBtnDidClick
{

    [self pickerViewIsOnView];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"hh:mm"];
    
    
    self.startPicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, ScreenHeight - 200, ScreenWidth, 200)];
    [self.startPicker setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [self.startPicker setDatePickerMode:UIDatePickerModeTime];
    [self.startPicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_Hans_CN"]];
    NSDate *startDate= [myDateFormatter dateFromString:self.timeLabel.text];
    [self.startPicker setDate:startDate animated:YES];
    [self.startPicker addTarget:self action:@selector(startPickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.startPicker];


}


- (void) startPickerValueChanged:(UIDatePicker *)picker
{
    NSDate *select = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    self.timeLabel.text = dateAndTime;
}



- (void) endBtnDidClick
{
    [self pickerViewIsOnView];

    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"hh:mm"];

    
    self.endPicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, ScreenHeight - 200, ScreenWidth, 200)];
    [self.endPicker setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [self.endPicker setDatePickerMode:UIDatePickerModeTime];
    [self.endPicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_Hans_CN"]];
    [self.endPicker addTarget:self action:@selector(endPickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    if([self.endTimeLabel.text isEqualToString:@""]){
        [self.endPicker setDate:[NSDate date] animated:YES];
    }else{
        NSDate *endtDate= [myDateFormatter dateFromString:self.endTimeLabel.text];
        [self.endPicker setDate:endtDate animated:YES];
    }
    
    [self.view addSubview:self.endPicker];

}


- (void) endPickerValueChanged:(UIDatePicker *)picker
{
    NSDate *select = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    self.endTimeLabel.text = dateAndTime;

}


- (void) pickerViewIsOnView
{
    if(self.startPicker != nil){
        [self.startPicker removeFromSuperview];
    }
    
    if(self.datePicker != nil){
        [self.datePicker removeFromSuperview];
    }
    
    if(self.endPicker != nil){
        [self.endPicker removeFromSuperview];
    }
    
}




- (void) sendRequestForOrderRoom
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/MeetingServ";
    
    NSString *roomName = self.model.roomName;
    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    
//    2015-01-01_00-00-00
    NSLog(@"date -- %@",self.dateLabel.text);
    
    NSString *date1 = [self.dateLabel.text stringByReplacingOccurrencesOfString:@"年" withString:@"-"];
    NSString *date2 = [date1 stringByReplacingOccurrencesOfString:@"月" withString:@"-"];
    NSString *date3 = [date2 stringByReplacingOccurrencesOfString:@"日" withString:@"_"];

    NSString *startTime = [self.timeLabel.text stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *endTime = [self.endTimeLabel.text stringByReplacingOccurrencesOfString:@":" withString:@"-"];

    NSString *startStr = [NSString stringWithFormat:@"%@%@-00",date3,startTime];
    NSString *endStr = [NSString stringWithFormat:@"%@%@-00",date3,endTime];

    NSString *theme = self.theme.text;
    NSString *content = self.content.text;
    
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"1";
    parameters[@"roomName"] = roomName;
    parameters[@"uid"] = uid;
    parameters[@"startTime"] = startStr;
    parameters[@"endTime"] = endStr;
    parameters[@"theme"] = theme;
    parameters[@"content"] = content;


    
    NSLog(@"roomName -- %@",roomName);
    NSLog(@"parameters -- %@",parameters);
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];//使用这个将得到的是JSON
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSString *res = [string substringFromIndex:string.length - 1];
        NSLog(@"res -- %@",res);
        if([res intValue] == 1){
            
            [self showMessageOnView:@"预定成功"];
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                ; });

        }else
        {

        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    
}






- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
