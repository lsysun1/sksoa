//
//  OA_CancelMeetViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/18.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_CancelMeetViewController.h"

@interface OA_CancelMeetViewController ()
@property (strong, nonatomic) IBOutlet UIView *bgView;

@end

@implementation OA_CancelMeetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo)];
    
    [self.bgView addGestureRecognizer:tapGesture];
    
}

- (void) Actiondo
{
    [self.view removeFromSuperview];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
