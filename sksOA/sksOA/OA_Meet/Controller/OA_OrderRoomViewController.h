//
//  OA_OrderRoomViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_BaseViewController.h"
@class OA_MeetList;
@interface OA_OrderRoomViewController : OA_BaseViewController
@property (nonatomic,copy) NSString *roomID;
@property (nonatomic ,strong) OA_MeetList *model;
@end
