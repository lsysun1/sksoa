//
//  OA_OrderRoomViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_OrderRoomViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "OA_OrderBtn.h"
#import "OA_ConfirmOrderViewController.h"
#import "OA_MeetList.h"
#import "OA_MeetInfo.h"
#import "OA_CancelBtn.h"
#import "OA_CancelMeetViewController.h"

@interface OA_OrderRoomViewController () <UITableViewDataSource,UITableViewDelegate,OA_OrderBtnDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *monday;
@property (weak, nonatomic) IBOutlet UILabel *tuesday;
@property (weak, nonatomic) IBOutlet UILabel *wednesday;
@property (weak, nonatomic) IBOutlet UILabel *thursday;
@property (weak, nonatomic) IBOutlet UILabel *friday;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSDate *day1Date;
@property (strong, nonatomic) NSDate *day2Date;
@property (strong, nonatomic) NSDate *day3Date;
@property (strong, nonatomic) NSDate *day4Date;
@property (strong, nonatomic) NSDate *day5Date;
@property (strong, nonatomic) NSMutableArray *meetInfoArr;

@property (strong, nonatomic) UIView *cancelView;
@property (strong, nonatomic) UILabel *cancelTime;
@property (strong, nonatomic) UILabel *proposer;
@property (strong, nonatomic) UILabel *participant;
@property (strong, nonatomic) UIButton *cancelBtn;
@property (strong, nonatomic) UIButton *linkBtn;
@property (strong, nonatomic) UILabel *themeLabel;
@property (strong, nonatomic) UILabel *contentLabel;


@property (weak, nonatomic) IBOutlet UILabel *meetName;
@property (weak, nonatomic) IBOutlet UILabel *peopleNum;
@property (weak, nonatomic) IBOutlet UILabel *equipment;
@property (weak, nonatomic) IBOutlet UILabel *mater;


@property (copy, nonatomic) NSString *cancelEndTime;

@end

@implementation OA_OrderRoomViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.meetInfoArr = [[NSMutableArray alloc]init];
    
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self setUpWeekLabel];
    [self setUpScrollView];
    
    
    [self setUpTable];
    [self sendRequestForInfo];

    self.meetName.text = self.model.roomName;
    self.peopleNum.text = self.model.capacity;
    self.equipment.text = self.model.equipment;
    self.mater.text = self.model.responsor;
    
    
}

- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - 时间Label设置
- (void)setUpWeekLabel{
    NSDate *newDate = [NSDate date];
    if (newDate == nil) {
        newDate = [NSDate date];
    }
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:2];//设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitWeekOfMonth startDate:&beginDate interval:&interval forDate:newDate];
    //分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval-1];
    }else {
        return;
    }
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"MM.dd"];
    NSString *beginString = [myDateFormatter stringFromDate:beginDate];
    NSString *endString = [myDateFormatter stringFromDate:endDate];

    
    
    NSString *s = [NSString stringWithFormat:@"%@-%@",beginString,endString];
    NSLog(@"%@",s);
    
    self.day1Date = beginDate;
    self.day2Date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([beginDate timeIntervalSinceReferenceDate] + 24*3600)];
    self.day3Date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([self.day2Date timeIntervalSinceReferenceDate] + 24*3600)];
    self.day4Date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([self.day3Date timeIntervalSinceReferenceDate] + 24*3600)];
    self.day5Date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([self.day4Date timeIntervalSinceReferenceDate] + 24*3600)];

    [self setTimeViewWith:self.day1Date Label:self.monday];
    [self setTimeViewWith:self.day2Date Label:self.tuesday];
    [self setTimeViewWith:self.day3Date Label:self.wednesday];
    [self setTimeViewWith:self.day4Date Label:self.thursday];
    [self setTimeViewWith:self.day5Date Label:self.friday];

    
    
}


- (void) setTimeViewWith:(NSDate *)date Label:(UILabel *)label
{
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"MM.dd"];
    NSString *timeStr = [myDateFormatter stringFromDate:date];
    
    int month;
    int day;
    
    NSString *monthTmp = [timeStr substringToIndex:2];
    NSString *monthFinal = [[NSString alloc]init];
    NSRange monthObj=[monthTmp rangeOfString:@"0"];
    if(monthObj.location != NSNotFound){
        monthFinal = [monthTmp substringFromIndex:monthObj.location+1];
        month = [monthFinal intValue];
    }else{
        month = [monthTmp intValue];
    }
    
    
    
    NSString *dayTmp = [timeStr substringFromIndex:3];
    NSLog(@"dayTmp --- %@",dayTmp);
    NSString *dayFinal = [[NSString alloc]init];
    NSRange dayObj=[dayTmp rangeOfString:@"0"];
    if(dayObj.location != NSNotFound){
            if(dayObj.location == 0){
                dayFinal = [dayTmp substringFromIndex:dayObj.location+1];
                day = [dayFinal intValue];
            }else{
                day = [dayTmp intValue];
            }
    }else{
        day = [dayTmp intValue];
    }
    
    
    label.text = [NSString stringWithFormat:@"%d月%d日",month,day];
    
}

#pragma mark - 设置_scrollView
- (void) setUpScrollView
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 220, ScreenWidth, ScreenHeight - 220)];
    self.scrollView.contentSize = CGSizeMake(ScreenWidth, 800);
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    self.scrollView.bounces = NO;
    [self.view addSubview:self.scrollView];
    
    CGFloat labelHeight = 0;
    for (int i = 0; i <= 15; i++) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, labelHeight, 20, 50)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:11];
        label.text = [NSString stringWithFormat:@"%d",8+i];
        [self.scrollView addSubview:label];
        labelHeight += 50;
    }
}


#pragma mark - 设置tableView及其代理方法
- (void) setUptableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 30, 800)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 50;
    [self.tableView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
//    self.tableView.separatorStyle = 0;
    [self.scrollView addSubview:self.tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectio
{
    return 16;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = @"123";
    return cell;
}



- (void) setUpTable
{
    UIView *myTableView = [[UIView alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 30, 800)];
    [myTableView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [self.scrollView addSubview:myTableView];
    

    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    int hourTime = 8;
    for(int i = 1; i <= 80; i++){
        OA_OrderBtn *btn = [[OA_OrderBtn alloc]initWithFrame:CGRectMake(btnX, btnY, (ScreenWidth - 30) / 5 - 1, 50 -1)];
        btn.backgroundColor = [UIColor lightGrayColor];
        btn.tag = i;
        btn.delegate = self;
        btn.hourTime = hourTime;
        [myTableView addSubview:btn];
        
        if(i % 5 == 0){
            btnY += 50;
            hourTime += 1;
        }
        
        btnX += (ScreenWidth - 30) / 5;
        if((i+1) % 5 == 1){
            btnX = 0;
        }
    }

}


#pragma mark - OA_OrderBtn Delegate Method
- (void) orderBtnHourTime:(int)hour MinTime:(NSString *) min Tag:(NSInteger) tag;
{
    OA_ConfirmOrderViewController *confrimVC = [[OA_ConfirmOrderViewController alloc]init];
    confrimVC.model = self.model;
    
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"YYYY年MM月dd日"];
    
    switch (tag % 5) {
        case 1:
        {
            NSString *dateStr = [myDateFormatter stringFromDate:self.day1Date];
            confrimVC.meetDateStr = dateStr;
            confrimVC.date = self.day1Date;
        }
            break;
        case 2:
        {
            NSString *dateStr = [myDateFormatter stringFromDate:self.day2Date];
            confrimVC.meetDateStr = dateStr;
            confrimVC.date = self.day2Date;
        }
            break;
        case 3:
        {
            NSString *dateStr = [myDateFormatter stringFromDate:self.day3Date];
            confrimVC.meetDateStr = dateStr;
            confrimVC.date = self.day3Date;
        }
            break;
        case 4:
        {
            NSString *dateStr = [myDateFormatter stringFromDate:self.day4Date];
            confrimVC.meetDateStr = dateStr;
            confrimVC.date = self.day4Date;
        }
            break;
        case 0:
        {
            NSString *dateStr = [myDateFormatter stringFromDate:self.day5Date];
            confrimVC.meetDateStr = dateStr;
            confrimVC.date = self.day5Date;
        }
            break;
        default:
            break;
    }

    NSString *hourStr;
    if(hour < 10){
        hourStr = [NSString stringWithFormat:@"0%d",hour];
    }else{
        hourStr = [NSString stringWithFormat:@"%d",hour];
    }
    
    
    NSString *timeStr = [NSString stringWithFormat:@"%@:%@",hourStr,min];
    confrimVC.meetTimeStr = timeStr;
    
    
    
    [self.navigationController pushViewController:confrimVC animated:YES];
}



- (void) sendRequestForInfo
{
    NSString *url = @"http://182.140.244.136:8090/SKSAO/MeetingServ";
    
    NSString *roomName = self.model.roomName;
    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"0";
    parameters[@"roomName"] = roomName;
    parameters[@"uid"] = uid;

    NSLog(@"roomName -- %@",roomName);
    NSLog(@"parameters -- %@",parameters);
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"res -- %@",responseObject);
        NSArray *resArr = responseObject;
        for(NSDictionary *dict in resArr){
            OA_MeetInfo *meet = [OA_MeetInfo objectWithKeyValues:dict];
            [self.meetInfoArr addObject:meet];
        }
        
        [self createMeetOrderInfoBtn];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];

}

- (void) createMeetOrderInfoBtn
{
    for(OA_MeetInfo *meet in self.meetInfoArr){
        int cow = [meet.day intValue] - 1;      //btn在第几列
        NSLog(@"cow -- %d",cow);

        NSString *start = meet.startTime;
        NSString *end = meet.endTime;
        NSString *theme = meet.theme;
        NSString *content = meet.content;
        
        
        

        NSLog(@"start -- %@",start);
        
        NSString *startTime = [meet.startTime substringFromIndex:11];
        NSString *startHour = [startTime substringToIndex:2];

        NSString *endTime = [meet.endTime substringFromIndex:11];
        NSString *endHour = [endTime substringToIndex:2];
        
        int row = [startHour intValue] - 8;     //btn在第几行
        NSLog(@"row -- %d",row);
        

        NSDictionary *timeDict = [self intervalFromLastDate:meet.startTime toTheDate:meet.endTime];     //开始时间和结束时间的差
        
        NSLog(@"小时差 -- %@",timeDict[@"hour"]);
        NSLog(@"分钟差 -- %@",timeDict[@"min"]);


        

        
        
        NSString *s_MinStr = [startTime  substringFromIndex:3];
        int s_Min = [[s_MinStr substringToIndex:5] intValue];
        
        NSString *e_MinStr = [endTime  substringFromIndex:3];
        int e_Min = [[e_MinStr substringToIndex:5] intValue];

        
//        int e_Min = [[end substringWithRange:NSMakeRange(3, 5)] intValue];
        NSLog(@"startTime -- %@",start);
        NSLog(@"meet.endTime -- %@",end);
        int s_Hour = [startHour intValue];
        
        int e_Hour = [endHour intValue];
        int hour = e_Hour - s_Hour;
        
        
        for(int i = 0; i<= hour;i++){
            CGFloat btnStarH = 0.0;
            if(0 <= s_Min && s_Min < 15){
                btnStarH = 0;
            }else if (15 <= s_Min && s_Min < 30){
                btnStarH = 12.5;
            }else if (30 <= s_Min && s_Min < 45){
                btnStarH = 25;
            }else{
                btnStarH = 37.5;
            }
            
            CGFloat btnEndH = 0.0;
            if(0 <= e_Min && e_Min < 15){
                btnEndH = 0;
            }else if (15 <= e_Min && e_Min < 30){
                btnEndH = 12.5;
            }else if (30 <= e_Min && e_Min < 45){
                btnEndH = 25;
            }else{
                btnEndH = 37.5;
            }
            
            
            if(i == 0){
                int btnTag = 5 * (row + i) + cow;
                UIView *btnView = [self.view viewWithTag:btnTag];
                OA_CancelBtn *addBtn = [[OA_CancelBtn alloc]initWithFrame:CGRectMake(0, btnStarH,  (ScreenWidth - 30) / 5 - 1, 49 - btnStarH )];
                addBtn.startTime = start;
                addBtn.endTime = end;
                addBtn.content = content;
                addBtn.theme = theme;
                addBtn.backgroundColor = UIColorFromRGB(0xce4b4c);
                [addBtn addTarget:self action:@selector(cancelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
                [btnView addSubview:addBtn];
            }else if(i == hour){
                int btnTag = 5 * (row + i) + cow;
                UIView *btnView = [self.view viewWithTag:btnTag];
                OA_CancelBtn *addBtn = [[OA_CancelBtn alloc]initWithFrame:CGRectMake(0, 0,  (ScreenWidth - 30) / 5 - 1, btnEndH )];
                addBtn.startTime = start;
                addBtn.endTime = end;
                addBtn.content = content;
                addBtn.theme = theme;
                addBtn.backgroundColor = UIColorFromRGB(0xd87c5f);
                [addBtn addTarget:self action:@selector(cancelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
                [btnView addSubview:addBtn];
                
                UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, btnEndH, (ScreenWidth - 30) / 5 - 1, 1)];
                bottomLine.backgroundColor = UIColorFromRGB(0xce4b4c);
                [addBtn addSubview:bottomLine];
                
            }else{
                int btnTag = 5 * (row + i) + cow;
                UIView *btnView = [self.view viewWithTag:btnTag];
                OA_CancelBtn *addBtn = [[OA_CancelBtn alloc]initWithFrame:CGRectMake(0, 0,  (ScreenWidth - 30) / 5 - 1, 49 )];
                addBtn.startTime = start;
                addBtn.endTime = end;
                addBtn.content = content;
                addBtn.theme = theme;
                [addBtn addTarget:self action:@selector(cancelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
                addBtn.backgroundColor = UIColorFromRGB(0xce4b4c);
                [btnView addSubview:addBtn];

            }
        }
        
    
    }

}



- (void) cancelBtnDidClick:(OA_CancelBtn *)btn
{
    [self createCancelView];
    self.cancelTime.text = [NSString stringWithFormat:@"时间:%@",[btn.startTime substringToIndex:btn.startTime.length -2]];
    self.cancelEndTime = btn.endTime;
    self.themeLabel.text = btn.theme;
    self.contentLabel.text = btn.content;
    

    NSLog(@"btn.startTime -- %@",btn.startTime);
    NSLog(@"btn.endTime -- %@",btn.endTime);
}




- (NSDictionary *)intervalFromLastDate: (NSString *) dateString1  toTheDate:(NSString *) dateString2
{
    NSArray *timeArray1=[dateString1 componentsSeparatedByString:@"."];
    dateString1=[timeArray1 objectAtIndex:0];
    
    
    NSArray *timeArray2=[dateString2 componentsSeparatedByString:@"."];
    dateString2=[timeArray2 objectAtIndex:0];
    
//    NSLog(@"%@.....%@",dateString1,dateString2);
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *d1=[date dateFromString:dateString1];
    
    NSTimeInterval late1=[d1 timeIntervalSince1970]*1;
    
    
    
    NSDate *d2=[date dateFromString:dateString2];
    
    NSTimeInterval late2=[d2 timeIntervalSince1970]*1;
    
    
    
    NSTimeInterval cha=late2-late1;
    NSString *hour=@"";
    NSString *min=@"";
    NSString *sec=@"";
    
    sec = [NSString stringWithFormat:@"%d", (int)cha%60];
    //        min = [min substringToIndex:min.length-7];
    //    秒
    sec=[NSString stringWithFormat:@"%@", sec];
    
    
    
    min = [NSString stringWithFormat:@"%d", (int)cha/60%60];
    //        min = [min substringToIndex:min.length-7];
    //    分
    min=[NSString stringWithFormat:@"%@", min];
    
    
    //    小时
    hour = [NSString stringWithFormat:@"%d", (int)cha/3600];
    //        house = [house substringToIndex:house.length-7];
    hour=[NSString stringWithFormat:@"%@", hour];
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:hour forKey:@"hour"];
    [dict setObject:min forKey:@"min"];
    [dict setObject:sec forKey:@"sec"];

    
    return dict;
}




- (void) createCancelView
{
    self.cancelView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth)];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.5;
    [self.cancelView addSubview:bgView];
    
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelViewRemove)];
    [self.cancelView addGestureRecognizer:tapGesture];
    [self.view addSubview:self.cancelView];
    
    
    UIImageView *bgImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 192, 300, 184)];
    bgImg.image = [UIImage imageNamed:@"cancenl"];
    [self.cancelView addSubview:bgImg];
    
    self.themeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 190, 300, 25)];
    self.themeLabel.textAlignment = NSTextAlignmentCenter;
    self.themeLabel.text = @"会议主题";
    self.themeLabel.font = [UIFont boldSystemFontOfSize:17];
    [self.cancelView addSubview:self.themeLabel];
    
    
    
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(10, 213, 300, 5)];
    topLine.backgroundColor = [UIColor whiteColor];
    [self.cancelView addSubview:topLine];
    
    
    self.contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(23, 239, 200, 20)];
    self.contentLabel.text = @"会议内容";
    [self.cancelView addSubview:self.contentLabel];
    

    
    self.cancelTime = [[UILabel alloc]initWithFrame:CGRectMake(23, 277, 207, 20)];
    self.cancelTime.font = [UIFont systemFontOfSize:15];
    [self.cancelView addSubview:self.cancelTime];
    

    
    
    UIView *botomLine = [[UIView alloc]initWithFrame:CGRectMake(10, 333, 300, 5)];
    botomLine.backgroundColor = [UIColor whiteColor];
    [self.cancelView addSubview:botomLine];
    
    self.cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(23, 346, 110+160, 22)];
    self.cancelBtn.backgroundColor = [UIColor whiteColor];
    self.cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cancelBtn setTitle:@"取消会议" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelBtnComfrimDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelView addSubview:self.cancelBtn];
    
//    self.linkBtn = [[UIButton alloc]initWithFrame:CGRectMake(182, 346, 110, 22)];
//    self.linkBtn.backgroundColor = [UIColor whiteColor];
//    self.linkBtn.titleLabel.font = [UIFont systemFontOfSize:15];
//    [self.linkBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [self.linkBtn setTitle:@"联系申请人" forState:UIControlStateNormal];
//    [self.cancelView addSubview:self.linkBtn];
    
    

}



- (void) cancelBtnComfrimDidClick: (UIButton *)btn
{
    [self sendRequsetForCancelMeet];
}


- (void) sendRequsetForCancelMeet
{
    //    http://182.140.244.136:8090/SKSAO/LoginServ?username=duoyu&pwd=duoyu
    NSString *url = @"http://182.140.244.136:8090/SKSAO/MeetingServ";
    
    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];

    NSString *time1 = self.cancelTime.text;
    
    NSString *time2 = [time1 stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *time3 = [time2 stringByReplacingOccurrencesOfString:@" " withString:@"_"];

    
    NSLog(@"time --- %@",time3);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"query"] = @"3";
    parameters[@"uid"] = uid;
    parameters[@"startTime"] = time3;

    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];//使用这个将得到的是JSON
    
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSLog(@"res -- %@",responseObject);
        [self showMessageOnView:@"取消会议成功"];
        [self.cancelView removeFromSuperview];
        NSLog(@"res -- %@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    
    
}


- (void) cancelViewRemove
{
    [self.cancelView removeFromSuperview];
}


@end
