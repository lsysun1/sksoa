//
//  OA_OrderBtn.m
//  sksOA
//
//  Created by 李松玉 on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_OrderBtn.h"

@interface OA_OrderBtn()
{
    NSString *_min;
}
@end

@implementation OA_OrderBtn



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    //  2.获取当前触摸点
    CGPoint current = [touch locationInView:self];

    CGFloat btnY = current.y;

//    NSLog(@"btnY - %lf",btnY);
    NSDecimalNumber *myOtherDecimalObj = [[NSDecimalNumber alloc] initWithFloat:btnY];
    int yINT = [myOtherDecimalObj intValue];
    
//    NSLog(@"yINT - %d",yINT);
    
    if(0 <= yINT && yINT <= 12){
        _min = @"00";
    }else if (12 < yINT && yINT <= 25){
        _min = @"15";
    }else if (25 < yINT && yINT <= 37){
        _min = @"30";
    }else if (37 < yINT && yINT <= 49){
        _min = @"45";
    }
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderBtnHourTime:MinTime:Tag:)]){
        [self.delegate orderBtnHourTime:self.hourTime MinTime:_min Tag:self.tag];
    }

    
    
}



@end
