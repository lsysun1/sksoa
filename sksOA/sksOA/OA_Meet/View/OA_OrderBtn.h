//
//  OA_OrderBtn.h
//  sksOA
//
//  Created by 李松玉 on 15/5/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OA_OrderBtnDelegate <NSObject>

- (void) orderBtnHourTime:(int)hour MinTime:(NSString *) min Tag:(NSInteger) tag;

@end

@interface OA_OrderBtn : UIButton
@property (nonatomic ,assign) int hourTime;
@property (nonatomic, assign) id <OA_OrderBtnDelegate> delegate;
@end
