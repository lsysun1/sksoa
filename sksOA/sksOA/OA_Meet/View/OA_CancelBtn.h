//
//  OA_CancelBtn.h
//  sksOA
//
//  Created by 李松玉 on 15/5/18.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OA_CancelBtn : UIButton


@property (nonatomic ,copy) NSString *startTime;
@property (nonatomic ,copy) NSString *endTime;
@property (nonatomic,copy) NSString *theme;
@property (nonatomic,copy) NSString *content;

@end
