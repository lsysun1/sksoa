//
//  OA_MeetCell.h
//  sksOA
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OA_MeetList;

@interface OA_MeetCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;

@property (nonatomic,strong) OA_MeetList *meetList;

@end
