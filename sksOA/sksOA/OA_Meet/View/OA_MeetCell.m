//
//  OA_MeetCell.m
//  sksOA
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_MeetCell.h"
#import "OA_MeetList.h"

@interface OA_MeetCell()
@property (strong, nonatomic) IBOutlet UILabel *numLabel;
@property (strong, nonatomic) IBOutlet UILabel *equipmentLabel;
@property (strong, nonatomic) IBOutlet UILabel *adminName;
@property (weak, nonatomic) IBOutlet UILabel *meetName;


@end

@implementation OA_MeetCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"OA_MeetCell";
    OA_MeetCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"OA_MeetCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void) setMeetList:(OA_MeetList *)meetList
{
//    self.meetList = meetList;
    
    self.numLabel.text = meetList.capacity;
    self.adminName.text = meetList.responsor;
    self.equipmentLabel.text = meetList.equipment;
    self.meetName.text = meetList.roomName;
}


- (void)awakeFromNib {
    // Initialization code
    
//    self.meetName.frame = CGRectMake(ScreenWidth - 120, 0, 90, 30);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
