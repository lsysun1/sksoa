//
//  OA_HomeViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_HomeViewController.h"
#import "OA_NewsViewController.h"
#import "OA_ZoneViewController.h"
#import "OA_ContactViewController.h"
#import "OA_MeetViewController.h"
#import "OA_LoginViewController.h"

@interface OA_HomeViewController ()<UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIButton *newsBtn;
@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
@property (weak, nonatomic) IBOutlet UIButton *zoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *meetBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong,nonatomic) UIView *setView;


@property (weak, nonatomic) IBOutlet UIButton *setBtn;


@end

@implementation OA_HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [_newsBtn addTarget:self action:@selector(newsBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_contactBtn addTarget:self action:@selector(contactBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_zoneBtn addTarget:self action:@selector(zoneBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_meetBtn addTarget:self action:@selector(meetBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _contentScrollView.contentSize =  CGSizeMake(320, self.view.frame.size.height+5);

    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (void)newsBtnDidClick:(UIButton *)btn
{
    OA_NewsViewController *newsVC = [[OA_NewsViewController alloc]init];
    [self.navigationController pushViewController:newsVC animated:YES];
}

- (void)contactBtnDidClick:(UIButton *)btn
{
    OA_ContactViewController *contactVC = [[OA_ContactViewController alloc]init];
    [self.navigationController pushViewController:contactVC animated:YES];
}

- (void)zoneBtnDidClick:(UIButton *)btn
{
    OA_ZoneViewController *zoneVC = [[OA_ZoneViewController alloc]init];
    [self.navigationController pushViewController:zoneVC animated:YES];}

- (void)meetBtnDidClick:(UIButton *)btn
{
    OA_MeetViewController *meetVC = [[OA_MeetViewController alloc]init];
    [self.navigationController pushViewController:meetVC animated:YES];
}

- (IBAction)setBtnDidClick:(UIButton *)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"注销"
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:@"确定"
                                  otherButtonTitles:nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSLog(@"注销");
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userID"];
        OA_LoginViewController *VC = [[OA_LoginViewController alloc]init];
        [self.navigationController pushViewController:VC animated:YES];
        
    }
}


@end
