//
//  OA_BaseViewController.h
//  sksOA
//
//  Created by 李松玉 on 15/5/5.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJExtension.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFURLResponseSerialization.h"

@interface OA_BaseViewController : UIViewController



- (UIImage *)ImageFromColor:(UIColor *)color;
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image;
- (void) showMessageOnView:(NSString *)str;
@end
