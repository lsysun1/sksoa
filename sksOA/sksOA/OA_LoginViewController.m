//
//  OA_LoginViewController.m
//  sksOA
//
//  Created by 李松玉 on 15/5/17.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "OA_LoginViewController.h"
#import "OA_HomeViewController.h"

@interface OA_LoginViewController () <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *headImgView;
@property (strong, nonatomic) IBOutlet UIView *inputView;
@property (strong, nonatomic) UITextField *userName;
@property (strong, nonatomic) UITextField *passWord;
@property (strong, nonatomic) IBOutlet UIButton *logInBtn;

@end

@implementation OA_LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = YES;
    self.inputView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];

    self.userName = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, 310, 49)];
    self.userName.delegate = self;
    self.userName.placeholder = @"账号,测试账号用duoyu";
    [self.inputView addSubview:self.userName];
    
    self.passWord = [[UITextField alloc]initWithFrame:CGRectMake(10, 50, 310, 50)];
    self.passWord.delegate = self;
    [self.passWord setSecureTextEntry:YES];
    self.passWord.placeholder = @"密码,测试密码用duoyu";
    [self.inputView addSubview:self.passWord];
    
    [self.logInBtn setTitle:@"登录" forState:UIControlStateNormal];
    [self.logInBtn addTarget:self action:@selector(logInBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logInBtn];
    
}


- (void) logInBtnDidClick
{
    [self sendRequsetForLogIn];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //使textField取消第一响应者，从而隐藏键盘
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void) sendRequsetForLogIn
{
//    http://182.140.244.136:8090/SKSAO/LoginServ?username=duoyu&pwd=duoyu
    NSString *url = @"http://182.140.244.136:8090/SKSAO/LoginServ";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = self.userName.text;
    parameters[@"pwd"] = self.passWord.text;
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];//使用这个将得到的是JSON

    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
//        NSLog(@"res -- %@",responseObject);
        NSLog(@"res -- %@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];

        NSString *res = [string substringToIndex:8];
        NSString *lastres = [res substringFromIndex:res.length  - 1];
        if([lastres intValue] == 1){
            [self responseObjectToModel:string];
            [self.navigationController pushViewController:[[OA_HomeViewController alloc]init] animated:YES];
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error -- %@",error);
    }];
    
    
}

- (void) responseObjectToModel:(NSString *)str
{
    
    NSRange range1 = [str rangeOfString:@"contract department update"];;
    NSString *res1 = [str substringFromIndex:range1.location+27];
    NSRange range2 = [res1 rangeOfString:@";"];;
    NSString *res2 = [res1 substringToIndex:range2.location];
    NSLog(@"contract department update = %@",res2);
    [[NSUserDefaults standardUserDefaults] setObject:res2 forKey:@"department"];
    
    
    NSRange range3 = [str rangeOfString:@"contract update"];;
    NSString *res3 = [str substringFromIndex:range3.location + 16];
    NSRange range4 = [res3 rangeOfString:@";"];;
    NSString *res4 = [res3 substringToIndex:range4.location];
    NSLog(@"contract update = %@",res4);
    [[NSUserDefaults standardUserDefaults] setObject:res4 forKey:@"contract"];


    
    
    NSRange range5 = [str rangeOfString:@"room infor"];;
    NSString *res5 = [str substringFromIndex:range5.location+11];
    NSRange range6 = [res5 rangeOfString:@";"];;
    NSString *res6 = [res5 substringToIndex:range6.location];
    NSLog(@"room infor = %@",res6);
    [[NSUserDefaults standardUserDefaults] setObject:res6 forKey:@"room"];



    
    
    NSRange range7 = [str rangeOfString:@"userID"];;
    NSString *res7 = [str substringFromIndex:range7.location+ 7];
    NSLog(@"userID = %@",res7);
    [[NSUserDefaults standardUserDefaults] setObject:res7 forKey:@"userID"];


}


@end
